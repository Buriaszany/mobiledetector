package science.credo.mobiledetector2.resolvers

import android.graphics.Bitmap
import android.util.Size
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import science.credo.mobiledetector2.toPositiveInt
import android.graphics.BitmapFactory
import java.nio.ByteBuffer
import java.nio.IntBuffer


class BitmapUtils {
    companion object {

        public fun createBitmap(bitmapArray: IntArray, pixelPrecission: Int, width: Int, height: Int): Deferred<Bitmap> {

            return GlobalScope.async {

                val time = System.nanoTime()

                for (i in 0 until bitmapArray.size) {
                    val byte = bitmapArray[i]

                    val red = byte shl 16 and 0x00FF0000
                    val green = byte shl 8 and 0x0000FF00
                    val blue = byte and 0x000000FF
                    val result: Int = red or green or blue
                    bitmapArray[i] = result
                }

                println("============time> ${System.nanoTime() - time}   width:$width  height:$height")

                val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
                bitmap.setPixels(bitmapArray, 0, width, 0, 0, width, height)
                return@async bitmap
            }
        }

        public suspend fun createBitmap(data: ByteArray, pixelPrecission: Int, width: Int, height: Int): Bitmap {

            return GlobalScope.async {
//                val intArray = ByteArray(width * height) {
//                    val byte = data[it * pixelPrecission].toPositiveInt()
//                    val red = byte shl 16 and 0x00FF0000
//                    val green = byte shl 8 and 0x0000FF00
//                    val blue = byte and 0x000000FF
//                    red or green or blue
//                }
                val bb = ByteBuffer.wrap(data)
                val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
                bitmap.copyPixelsFromBuffer(bb)
//                bitmap.setPixels(intArray, 0, width, 0, 0, width, height)
                return@async bitmap
            }.await()

        }

        public fun createCropped(bitmap: Bitmap, startColumn: Int, startRow: Int, size: Int): Deferred<Bitmap> {
            return GlobalScope.async {
                return@async Bitmap.createBitmap(bitmap, startColumn, startRow, size, size)
            }
        }

        public fun getBitmap(bitmapdata: ByteArray,width :Int,height :Int) :Bitmap {
            println("================ $bitmapdata")
            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
            val bb :ByteBuffer = ByteBuffer.wrap(bitmapdata)
            bitmap.copyPixelsToBuffer(bb)
            return bitmap
        }

    }

}