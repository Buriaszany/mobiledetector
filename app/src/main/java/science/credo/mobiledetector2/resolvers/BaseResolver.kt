package science.credo.mobiledetector2.resolvers

import android.graphics.Bitmap
import science.credo.mobiledetector2.camera.DeviceConfiguration
import science.credo.mobiledetector2.camera.Frame

abstract class BaseResolver {
    abstract suspend fun resolve(
            frame: Frame,
            configuration: DeviceConfiguration
    ) : Triple<IntArray?,Bitmap?, Metadata?>

    var isBusy = false
}