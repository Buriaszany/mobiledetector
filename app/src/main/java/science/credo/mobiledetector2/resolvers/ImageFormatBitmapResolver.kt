package science.credo.mobiledetector2.resolvers

import android.graphics.Bitmap
import android.graphics.ImageFormat
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import science.credo.mobiledetector2.camera.Calibration
import science.credo.mobiledetector2.camera.DeviceConfiguration
import science.credo.mobiledetector2.camera.Frame
import science.credo.mobiledetector2.toPositiveInt
import science.credo.mobiledetector2.tools.Tools

class ImageFormatBitmapResolver : BaseResolver() {


    override suspend fun resolve(frame: Frame, configuration: DeviceConfiguration): Triple<IntArray?, Bitmap?, Metadata?> {

        return GlobalScope.async {

            println("==== IMAGE size ====> ${frame.width}  x ${frame.height}")


            val time = System.nanoTime()

            val bitmapArray = IntArray(configuration.scaledWidth * configuration.scaledHeight)


            val pixelPrecision = if (configuration.imageFormat == ImageFormat.RAW_SENSOR) 2 else 1
            val scaleWidth = frame.width / configuration.scaledWidth
            val scaleHeight = frame.height / configuration.scaledHeight
            val scale = scaleWidth * scaleHeight

            for (r in 0 until frame.height) {
                val indexRow = r * frame.width * pixelPrecision
                val scaledIndexRow = r / scaleWidth * configuration.scaledWidth
                var c = 0
                while (c < frame.width * pixelPrecision) {
                    val index = indexRow + c
                    val newIndex = scaledIndexRow + c / pixelPrecision / scaleHeight
                    val byte = frame.byteArray[index].toPositiveInt()
                    bitmapArray[newIndex] = bitmapArray[newIndex] + byte
                    c += pixelPrecision
                }
            }

            var sum = 0L
            var min = 255
            var max = 0
            var maxIndex = 0
            for (i in 0 until bitmapArray.size) {

                val avg = bitmapArray[i] / scale
                sum += avg

                val red = avg shl 16 and 0x00FF0000
                val green = avg shl 8 and 0x0000FF00
                val blue = avg and 0x000000FF
                val result: Int = red or green or blue
                bitmapArray[i] = result

                if (avg < min) {
                    min = avg
                }
                if (avg > max) {
                    max = avg
                    maxIndex = i
                }
            }

            val noiseAvg = (sum / bitmapArray.size).toInt()
            val x = maxIndex % configuration.scaledWidth
            val y = maxIndex / configuration.scaledWidth

            val metadata = Metadata(
                    x,
                    y,
                    noiseAvg,
                    max,
                    maxIndex,
                    configuration.scaledWidth,
                    configuration.scaledHeight)

            val bitmap = BitmapUtils.createBitmap(bitmapArray, pixelPrecision, configuration.scaledWidth, configuration.scaledHeight).await()
            return@async Triple(null, bitmap, metadata)


        }.await()


    }

}