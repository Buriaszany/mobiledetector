package science.credo.mobiledetector2.resolvers

import android.graphics.Bitmap
import android.graphics.ImageFormat
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import science.credo.mobiledetector2.camera.Calibration
import science.credo.mobiledetector2.camera.DeviceConfiguration
import science.credo.mobiledetector2.camera.Frame
import science.credo.mobiledetector2.toPositiveInt
import science.credo.mobiledetector2.tools.Statistics
import java.util.*

class DetectionResolver : BaseResolver() {

    val thresholdQueue : Deque<Int> = LinkedList()

    override suspend fun resolve(
            frame: Frame,
            configuration: DeviceConfiguration
    ): Triple<IntArray?, Bitmap?, Metadata?> {
        return GlobalScope.async {

            if (configuration.isLossy) {
                isBusy = true
            }

            val time = System.nanoTime()

            println("========== ${configuration.scaledWidth}  ${configuration.scaledHeight}  ")

            val bitmapArray = IntArray(configuration.scaledWidth * configuration.scaledHeight)
            val pixelPrecision = if (configuration.imageFormat == ImageFormat.RAW_SENSOR) 2 else 1


            val scaleWidth = frame.width / configuration.scaledWidth
            val scaleHeight = frame.height / configuration.scaledHeight
            val scale = scaleWidth * scaleHeight

            println("========== ${scaleWidth}  ${scaleHeight}  ${scale}")

            for (r in 0 until frame.height) {
                val indexRow = r * frame.width * pixelPrecision
                val scaledIndexRow = r / scaleHeight * configuration.scaledWidth
                var c = 0
                while (c < frame.width * pixelPrecision) {
                    val index = indexRow + c
                    val newIndex = scaledIndexRow + c / pixelPrecision / scaleWidth
                    val byte = frame.byteArray[index].toPositiveInt()
                    bitmapArray[newIndex] = bitmapArray[newIndex] + byte
//                    println("========== ${newIndex}")
                    c += pixelPrecision
                }
            }

            var sum = 0L
            var min = 255
            var max = 0
            var maxIndex = 0
            for (i in 0 until bitmapArray.size) {

                val avg = bitmapArray[i] / scale
                sum += avg

                val red = avg shl 16 and 0x00FF0000
                val green = avg shl 8 and 0x0000FF00
                val blue = avg and 0x000000FF
                val result: Int = red or green or blue
                bitmapArray[i] = result

                if (avg < min) {
                    min = avg
                }
                if (avg > max) {
                    max = avg
                    maxIndex = i
                }
            }

            val noiseAvg = (sum / bitmapArray.size).toInt()
            val x = maxIndex % configuration.scaledWidth
            val y = maxIndex / configuration.scaledWidth

            val metadata = Metadata(
                    x,
                    y,
                    noiseAvg,
                    max,
                    maxIndex,
                    configuration.scaledWidth,
                    configuration.scaledHeight)

            if (max > Calibration.detectionThreshold && noiseAvg < Calibration.noiseThreshold) {
                metadata.hit = true
                val bitmap = BitmapUtils.createBitmap(frame.byteArray, pixelPrecision, frame.width, frame.height)
//            val bitmap = BitmapUtils.getBitmap(frame.byteArray,frame.width,frame.height)
                val clusteredBitmap = BitmapUtils.createBitmap(bitmapArray, 0, configuration.scaledWidth, configuration.scaledHeight)
                metadata.clusteredBitmap = clusteredBitmap.await()
                val croppedSize = 70
                var startRow = (y * scaleHeight) - (croppedSize / 2)
                var startColumn = (x * scaleWidth) - (croppedSize / 2)

                if (startColumn + croppedSize > bitmap.width) {
                    startColumn = bitmap.width - croppedSize
                }
                if (startColumn < 0) {
                    startColumn = 0
                }

                if (startRow + croppedSize > bitmap.height) {
                    startRow = bitmap.height - croppedSize
                }
                if (startRow < 0) {
                    startRow = 0
                }

                println("======================$max  $y   $x   $scaleHeight   $scaleWidth  ")

                val croppedBitmap = BitmapUtils.createCropped(bitmap, startColumn, startRow, croppedSize).await()

//            val original = IntArray(frame.width * frame.height)
//
//            for (i in 0 until original.size) {
//                original[i] = frame.byteArray[i * pixelPrecision].toPositiveInt()
//            }

                metadata.treshold = Calibration.detectionThreshold

//            val originalBitmap = BitmapUtils.createBitmap(original, 0, frame.width, frame.height)
                metadata.originalBitmap = bitmap
                return@async Triple(null, croppedBitmap, metadata)
            } else if(max <= Calibration.detectionThreshold && noiseAvg < Calibration.noiseThreshold){
                thresholdQueue.addFirst ((max*Calibration.AMPLIFIER).toInt())
                if(thresholdQueue.size>20){
                    thresholdQueue.removeLast()
                    Calibration.detectionThreshold = Statistics(thresholdQueue.toIntArray()).mean.toInt()
                    println("==============ready ${Calibration.detectionThreshold}")
                }else{
                    println("==============not ready ${Statistics(thresholdQueue.toIntArray()).mean.toInt()}")
                }
                return@async Triple(null, null, metadata)
            }else{
                return@async Triple(null, null, metadata)
            }

        }.await()
    }


}