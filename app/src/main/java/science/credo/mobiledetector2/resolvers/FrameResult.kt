package science.credo.mobiledetector2.resolvers

import android.graphics.Bitmap
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import kotlinx.coroutines.*
import science.credo.mobiledetector2.camera.DeviceConfiguration
import science.credo.mobiledetector2.camera.Frame
import science.credo.mobiledetector2.writer.BaseModel
import science.credo.mobiledetector2.writer.BaseWriter
import java.lang.IllegalStateException
import java.util.*

class FrameResult(
        private val resultResolver: BaseResolver,
        var writer: BaseWriter?
) : BaseModel() {


    constructor(resultResolver: BaseResolver) : this(resultResolver, null)


    @Expose
    var resultBitmap: Bitmap? = null
    @Expose
    var metadata: Metadata? = null
    @Expose
    var intArray: IntArray? = null


    public suspend fun resolve(frame: Frame, configuration: DeviceConfiguration) {
        val triple = resultResolver.resolve(frame, configuration)
        intArray = triple.first
        resultBitmap = triple.second
        metadata = triple.third
    }

    override fun write(): Job {

        if (writer == null) {
            throw IllegalStateException("Writer was not defined")
        } else {
            return GlobalScope.launch {
                writer!!.write(resultBitmap!!).join()
                writer!!.write(
                        GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(
                                metadata!!
                        )
                ).join()
                if(metadata!!.clusteredBitmap!=null){
                    writer!!.write(metadata!!.clusteredBitmap!!).join()
                }
                if(metadata!!.originalBitmap!=null){
                    writer!!.write(metadata!!.originalBitmap!!).join()
                }
            }
        }
    }

    override fun <T : Any> writeForResult(resultClass: Class<T>): Deferred<List<T>> {
        if (writer == null) {
            throw IllegalStateException("Writer was not defined")
        } else {
            return GlobalScope.async {
                val r1 = writer!!.writeForResult(resultBitmap!!, resultClass)
                val r2 = writer!!.writeForResult(metadata!!, resultClass)

                return@async listOf(r1, r2)
            }
        }
    }

    override fun toJson(): String {
        return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this)
    }

}