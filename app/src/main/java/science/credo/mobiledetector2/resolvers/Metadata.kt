package science.credo.mobiledetector2.resolvers

import android.graphics.Bitmap
import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import science.credo.mobiledetector2.writer.BaseModel
import science.credo.mobiledetector2.writer.BaseWriter
import java.lang.IllegalStateException

data class Metadata(
        @Expose
        val x: Int,
        @Expose
        val y: Int,
        @Expose
        val avg: Int,
        @Expose
        val max: Int,
        @Expose
        val maxIndex: Int,
        @Expose
        val scaledWidth: Int,
        @Expose
        val scaledHeight: Int,
        val writer: BaseWriter?
) {


    constructor(
            x: Int,
            y: Int,
            avg: Int,
            max: Int,
            maxIndex: Int,
            scaledWidth: Int,
            scaledHeight: Int
    ) : this(x, y, avg, max, maxIndex, scaledWidth, scaledHeight, null)

    constructor(
            avg: Int
    ) : this(-1, -1, avg, -1, -1, -1, -1, null)

    public var hit = false

    var clusteredBitmap : Bitmap? =null
    var originalBitmap : Bitmap? =null
    @Expose
    public var treshold = 0
}
