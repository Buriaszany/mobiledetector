package science.credo.mobiledetector2.tools

import android.app.AlertDialog
import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import science.credo.mobiledetector2.R

object UIUtils {

    fun showAlertDialog(context: Context, msg: String) {
        GlobalScope.launch(Dispatchers.Main) {
            AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.app_name))
                    .setMessage(msg)
                    .create()
                    .show()
        }
    }

}