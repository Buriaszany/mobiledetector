package science.credo.mobiledetector2.tools

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.graphics.drawable.ColorDrawable

import android.graphics.Color
import android.text.InputType
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import science.credo.mobiledetector2.R


class InputFragmentDialog : DialogFragment() {

    interface InputDialogCallback {
        fun onConfirm(v: String)
        fun onCancel()
    }
    companion object {
        fun newInstance(): InputFragmentDialog {
            return InputFragmentDialog()
        }
    }

    private lateinit var tvText: TextView
    private lateinit var etInput: EditText
    private lateinit var btApply: Button


    var text: String? = null
    var hint: String? = null
    var callback: InputDialogCallback? = null

    var isInputNumericalOnly = false

    fun setError(e :String){
        etInput.error = e
    }





    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.view_info_dialog, container, false)
        tvText = rootView.findViewById(R.id.tvText)
        etInput = rootView.findViewById(R.id.etInput)
        btApply = rootView.findViewById(R.id.btApply)

        if (text != null) {
            tvText.text = text
        }

        if (hint != null) {
            etInput.hint = hint
        }

        if(isInputNumericalOnly){
            etInput.inputType =InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
        }

        if (callback != null) {
            btApply.setOnClickListener {
                callback?.onConfirm(etInput.text.toString())
            }
        }

        return rootView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.dialog_theme)

    }

    override fun onStart() {
        super.onStart()
        val d = dialog
        if (d != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            d.window?.setLayout(width, height)
            d.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }




}