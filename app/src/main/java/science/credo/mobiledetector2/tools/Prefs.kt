package science.credo.mobiledetector2.tools

import android.content.Context
import com.google.gson.Gson


object Prefs {

    private const val PREFS_FILENAME = "science.credo.mobiledetector2.prefs"
    public const val DETECTION_COUNTER = "_det_count"


    fun <T : Any> put(context: Context, jsonString: String, cls: Class<T>, key: String) {
        val prefs = context.getSharedPreferences(PREFS_FILENAME, 0)
        val editor = prefs.edit()
        editor.putString(cls.simpleName + key, jsonString)
        editor.apply()
    }


    fun <T : Any> put(context: Context, obj: T) {
        put(context, obj, "")
    }

    fun <T : Any> put(context: Context, obj: T, key: String) {
        val prefs = context.getSharedPreferences(PREFS_FILENAME, 0)
        val editor = prefs.edit()
        editor.putString(
                obj::class.java.simpleName + key,
                Gson().toJson(obj))
        editor.apply()
    }

    fun <T> get(context: Context, cls: Class<T>): T? {
        return get(context, cls, "")
    }

    fun <T> get(context: Context, cls: Class<T>, key: String): T? {
        val prefs = context.getSharedPreferences(PREFS_FILENAME, 0)
        val json = prefs.getString(cls.simpleName + key, null)
        return Gson().fromJson(json, cls)
    }


}