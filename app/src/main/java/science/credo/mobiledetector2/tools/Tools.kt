package science.credo.mobiledetector2.tools

import android.graphics.ImageFormat
import java.util.*

class Tools {
    companion object {
        fun getDirName(timestamp: Long): String {
            val c = Calendar.getInstance()
            c.timeInMillis = timestamp
            return "/detections/" +
                    "${c.get(Calendar.YEAR)}-" +
                    "${c.get(Calendar.MONTH)+1}-" +
                    "${c.get(Calendar.DAY_OF_MONTH)}_" +
                    "${c.get(Calendar.HOUR_OF_DAY)}-" +
                    "${c.get(Calendar.MINUTE)}-" +
                    "${c.get(Calendar.SECOND)}"

        }

        fun getDirName(timestamp: Long,avg: Int): String {
            val c = Calendar.getInstance()
            c.timeInMillis = timestamp
            return "/detections_avg/${avg}_" +
                    "${c.get(Calendar.YEAR)}-" +
                    "${c.get(Calendar.MONTH)+1}-" +
                    "${c.get(Calendar.DAY_OF_MONTH)}_" +
                    "${c.get(Calendar.HOUR_OF_DAY)}-" +
                    "${c.get(Calendar.MINUTE)}-" +
                    "${c.get(Calendar.SECOND)}"

        }

        fun getImageFormatName(imageFormat: Int): String {
            return if (imageFormat == ImageFormat.RAW_SENSOR) {
                "RAW_SENSOR"
            } else if (imageFormat == ImageFormat.YUV_420_888) {
                "YUV_420_888"
            } else if (imageFormat == ImageFormat.JPEG) {
                "JPEG"
            } else {
                "UNKNOWN ($imageFormat)"
            }
        }
        fun nanosToMillisString(nanos: Long): String {
            return String.format("%.2f", nanos / 1000000.0) + "ms"
        }
    }
}