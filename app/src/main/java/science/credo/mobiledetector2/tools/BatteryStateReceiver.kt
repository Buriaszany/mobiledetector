package science.credo.mobiledetector2.tools

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.BatteryManager

class BatteryStateReceiver : BroadcastReceiver() {

    companion object {
        var temp: Int = 0;

    }

    override fun onReceive(context: Context, intent: Intent) {
        temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0)
//        println("======== temp change? ====> $temp")
    }
}