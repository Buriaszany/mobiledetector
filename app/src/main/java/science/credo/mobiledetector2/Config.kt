package science.credo.mobiledetector2


class Config {
    companion object {
        const val CAMERA_ID: Int = 0
        const val DIFFERENCE_TOLERANCE: Double = 0.05
        const val CALIBRATION_LENGHT: Int = 100
        const val BLACK_TRESHOLD: Int = 40

        const val RC_PERMISSIONS: Int = 123
        const val EXTRA_DEVICE_CONFIGURATION: String = "ex_dev_conf"
        const val EXTRA_START_CALIBRATION: String = "ex_start_cal"

    }
}

//extensions
fun Byte.toPositiveInt() = toInt() and 0xFF

