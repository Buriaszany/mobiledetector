package science.credo.mobiledetector2.writer

import kotlinx.coroutines.Job

abstract class BaseWriter {
    abstract suspend fun <T : Any> writeForResult(obj: Any, resultClass: Class<T>): T
    abstract fun write(obj: Any): Job
}