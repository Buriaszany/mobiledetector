package science.credo.mobiledetector2.writer

import android.graphics.Bitmap
import android.os.Environment
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.util.*

class FileWriter(
        var dir: String,
        var name: String
) : BaseWriter() {


    override suspend fun <T : Any> writeForResult(obj: Any, resultClass: Class<T>): T {

        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

    }


    var counter = 0
    override fun write(obj: Any): Job {

        counter++
        return GlobalScope.launch {
            val file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + dir
            val dir = File(file_path)
            if (!dir.exists())
                dir.mkdirs()


            var localName = "${counter}_" + name + if (obj is Bitmap) {
                ".png"
            } else {
                ".txt"
            }
            var file = File(dir, localName)
            if (file.exists()) {
                localName = name + "_full" + if (obj is android.graphics.Bitmap) {
                    ".png"
                } else {
                    ".txt"
                }
                file = File(dir, localName)
            }
            if (obj is Bitmap) {
                val fOut2 = FileOutputStream(file)
                obj.compress(Bitmap.CompressFormat.PNG, 100, fOut2)
                fOut2.flush()
                fOut2.close()
            } else {
                file.printWriter().use { out ->
                    out.println(obj)
                }
            }
        }

    }


}