package science.credo.mobiledetector2.writer

import com.google.gson.Gson
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.async

class WriteableObject<T:Any>(
        private val obj: T,
        private var writer: BaseWriter) : BaseModel() {



    fun changeWriter(writer: BaseWriter){
        this.writer = writer
    }

    override fun write(): Job {
        return writer.write(obj)
    }


    override fun <T : Any> writeForResult(resultClass: Class<T>): Deferred<List<T>> {
        return GlobalScope.async {
            return@async listOf(writer.writeForResult(obj,resultClass))
        }
    }



    override fun toJson(): String {
       return Gson().toJson(obj)
    }

}