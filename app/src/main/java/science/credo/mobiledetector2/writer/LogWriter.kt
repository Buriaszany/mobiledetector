package science.credo.mobiledetector2.writer

import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class LogWriter(val tag: String) : BaseWriter() {

    constructor() : this("")

    override suspend fun <T : Any> writeForResult(obj: Any, resultClass: Class<T>): T {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun write(obj: Any): Job {
        return GlobalScope.launch {
            println("$tag===> ${Gson().toJson(obj)}")
        }
    }
}