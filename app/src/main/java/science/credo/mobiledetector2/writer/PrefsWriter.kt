package science.credo.mobiledetector2.writer

import android.content.Context
import kotlinx.coroutines.*
import science.credo.mobiledetector2.tools.Prefs

class PrefsWriter<T :Any>(
        val context: Context,
        val key: String,
        private val clazz: Class<T>?
) : BaseWriter() {

    constructor(context: Context) : this(context, "",null)
    constructor(context: Context,clazz: Class<T>?) : this(context, "",clazz)


    override suspend fun <T : Any> writeForResult(obj: Any, resultClass: Class<T>): T {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun write(obj: Any): Job {
        return GlobalScope.launch {
            if(obj is String && clazz!=null){
                Prefs.put(context,obj,clazz,key)
            }else{
                Prefs.put(context, obj, key)
            }
        }
    }



}