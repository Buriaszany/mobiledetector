package science.credo.mobiledetector2.writer

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job

abstract class BaseModel {
    abstract fun write(): Job
    abstract fun <T:Any> writeForResult(resultClass: Class<T>): Deferred<List<T>>
    abstract fun toJson(): String
}