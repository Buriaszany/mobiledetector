package science.credo.mobiledetector2

import android.app.Application
import android.content.Intent
import android.content.IntentFilter
import science.credo.mobiledetector2.tools.BatteryStateReceiver

class App : Application(){

    val batteryReceiver: BatteryStateReceiver = BatteryStateReceiver()

    override fun onCreate() {
        super.onCreate()

        registerReceiver(batteryReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        registerReceiver(batteryReceiver, IntentFilter(Intent.ACTION_POWER_DISCONNECTED) )
        registerReceiver(batteryReceiver, IntentFilter(Intent.ACTION_BATTERY_LOW))
        registerReceiver(batteryReceiver, IntentFilter(Intent.ACTION_BATTERY_OKAY))
        registerReceiver(batteryReceiver, IntentFilter(Intent.ACTION_POWER_CONNECTED))
        registerReceiver(batteryReceiver, IntentFilter(Intent.ACTION_POWER_USAGE_SUMMARY))

    }


}