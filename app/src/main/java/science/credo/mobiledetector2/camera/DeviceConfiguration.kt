package science.credo.mobiledetector2.camera

import android.util.Size
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import science.credo.mobiledetector2.writer.BaseWriter
import science.credo.mobiledetector2.writer.BaseModel
import java.io.Serializable
import java.lang.IllegalStateException

data class DeviceConfiguration(
        @Expose
        var width: Int,
        @Expose
        var height: Int,
        @Expose
        var scaledWidth: Int,
        @Expose
        var scaledHeight: Int,
        @Expose
        val template: Int,
        @Expose
        var imageFormat: Int,
        @Expose
        val lowerIsoValue: Int,
        @Expose
        var sensorExposureTime: Long,
        @Expose
        var sensorMaxExposureTime: Long,
        @Expose
        var frameDuration: Long,
        @Expose
        val controlMode: Int,
        @Expose
        val controlAEMode: Int
) : BaseModel(), Serializable {

    @Expose
    val maxImages = 2

    @Expose
    var isLossy = false

    @Expose
    var avgCalculationTime = 0L

    @Transient
    var writer: BaseWriter? = null

    fun clone(): DeviceConfiguration {
        val stringProject = Gson().toJson(this, DeviceConfiguration::class.java)
        return Gson().fromJson<DeviceConfiguration>(stringProject, DeviceConfiguration::class.java)
    }

    override fun write(): Job {
        if (writer == null) {
            throw IllegalStateException("Writer was not defined")
        } else {
            return writer!!.write(toJson())
        }

    }

    override fun <T : Any> writeForResult(resultClass: Class<T>): Deferred<List<T>> {
        if (writer == null) {
            throw IllegalStateException("Writer was not defined")
        } else {
            return GlobalScope.async {
                return@async listOf(writer!!.writeForResult(toJson(), resultClass))
            }
        }
    }

    override fun toJson(): String {
        return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this)
    }


}