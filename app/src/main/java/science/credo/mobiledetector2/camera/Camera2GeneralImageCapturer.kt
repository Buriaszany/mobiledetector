package science.credo.mobiledetector2.camera

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageFormat
import android.media.ImageReader
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.util.Log
import kotlinx.coroutines.*
import java.io.File
import java.io.FileOutputStream
import android.R.attr.scaleHeight
import android.R.attr.scaleWidth

//
//@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//class Camera2GeneralImageCapturer(conf: Camera2ConfigurationUtil.DeviceConfiguration,
//                                  val desiredWidth: Int,
//                                  val desiredHeight: Int,
//                                  val callback: BitmapCallback)
//    : Camera2PostConfiguration(conf) {
//
//    interface BitmapCallback {
//        fun onBitmapSaved()
//    }
//
//    private val TAG = Camera2GeneralImageCapturer::class.java.simpleName
//    var counter = 0
//
//
//    override fun onImageAvailable(p0: ImageReader?) {
//
//        val image = p0!!.acquireLatestImage()
//        var width = 0
//        var height = 0
//        var imageFormat: Int
//        if (image != null) {
//            width = image.width
//            height = image.height
//            imageFormat = p0.imageFormat
//            val buffer1 = image.planes[0].buffer
//            var data1: ByteArray? = null
//            data1 = ByteArray(buffer1.remaining())
//            buffer1.get(data1)
//            counter++
//            println("$TAG  $counter")
//            if (counter == 3) {
//                counter = 0
//                stop()
//                save(data1, width, height)
//                callback.onBitmapSaved()
//
//            }
//            image.close()
//        }
//
//    }
//
//
//    private fun save(data: ByteArray, width: Int, height: Int) {
//
//        GlobalScope.launch(Dispatchers.Main) {
//            val bitmap = createBitmap(data, width, height).await()
//
//            val file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/detections"
//            val dir = File(file_path)
//            if (!dir.exists())
//                dir.mkdirs()
//            val name = getImageFormatName(conf.imageFormat) +
//                    "_${desiredWidth}x${desiredHeight}_" +
//                    System.currentTimeMillis()
//            val file = File(dir, "$name.png")
//            val fOut = FileOutputStream(file)
//
//            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut)
//            fOut.flush()
//            fOut.close()
//        }
//    }
//
//
//
//    private fun createBitmap(data: ByteArray, width: Int, height: Int): Deferred<Bitmap> {
//
//        return GlobalScope.async {
//            val bitmapArray: IntArray = IntArray(desiredWidth * desiredHeight)
//            val pixelPrecission = if (conf.imageFormat == ImageFormat.RAW_SENSOR) 2 else 1
//
//
//            println("============> $pixelPrecission  ${conf.imageFormat}")
//            println("============> ${bitmapArray.size}  ${data.size}")
//
//
//            val scaleWidth = width / desiredWidth
//            val scaleHeight = height / desiredHeight
//            val scale = scaleWidth * scaleHeight
//            println("============>scale ${scaleWidth}  ${scaleHeight}")
//
//            val time = System.nanoTime()
//            for (r in 0 until height) {
//                val indexRow = r * width * pixelPrecission
//                val scaledIndexRow = r / scaleWidth * desiredWidth
//                var c = 0
//                while (c < width * pixelPrecission) {
//                    val index = indexRow + c
//                    val newIndex = scaledIndexRow + c / pixelPrecission / scaleHeight
//                    val byte = data[index].toPositiveInt()
//                    bitmapArray[newIndex] = bitmapArray[newIndex] + byte
//                    c += pixelPrecission
//                }
//            }
//
//            for (i in 0 until bitmapArray.size) {
//                val byte = if(scale==1){
//                    bitmapArray[i]
//                }else{
//                    bitmapArray[i] / scale
//                }
//
//                val red = byte shl 16 and 0x00FF0000
//                val green = byte shl 8 and 0x0000FF00
//                val blue = byte and 0x000000FF
//                val result: Int = red or green or blue
//                bitmapArray[i] = result
//            }
//
//            println("============time> ${System.nanoTime() - time}")
//
////            for ((index, value) in (0 until data.size step step).withIndex()) {
////                val byte = data[value].toPositiveInt()
////            var red = byte shl 16 and 0x00FF0000
////            var green = byte shl 8 and 0x0000FF00
////            var blue = byte and 0x000000FF
////            val result: Int = red or green or blue;
////
////            bitmapArray[index] = result
////        }
//            val bitmap = Bitmap.createBitmap(desiredWidth, desiredHeight, Bitmap.Config.RGB_565)
//            bitmap.setPixels(bitmapArray, 0, desiredWidth, 0, 0, desiredWidth, desiredHeight)
//            return@async bitmap
//        }
//    }
//
//
//    fun getAveragedByteArray(
//            data: ByteArray,
//            width: Int,
//            height: Int
//    ): ByteArray {
//
//        val averaged: ByteArray = ByteArray(10)
//
////    Log.i("LOG", "=========frame size ${width * height}")
//        for (i in 0 until data.size step 2) {
//
//        }
////        val blacks: Double = zeros * 1000 / (width * height).toDouble()
////        val blacksAfterReduction: Double = zerosAfterNoiseReduction * 1000 / (width * height).toDouble()
////    val avgNonBlack = nonBlackSum / ((data.size / 2) - zeros)
////    val avgNonBlack = 0
//
//        return averaged
//    }
//
//}