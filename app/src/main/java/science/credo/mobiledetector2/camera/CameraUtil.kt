package science.credo.mobiledetector2.camera

import android.content.Context
import science.credo.mobiledetector2.camera.Frame

abstract class CameraUtil {

    interface FrameCallback {
        fun onFrameReceived(frame: Frame)
    }

    abstract fun start(context: Context)

    abstract fun stop()

}