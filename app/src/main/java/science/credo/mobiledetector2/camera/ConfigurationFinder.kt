package science.credo.mobiledetector2.camera

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.ImageFormat
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CameraMetadata
import android.hardware.camera2.params.StreamConfigurationMap
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import android.support.annotation.RequiresApi
import android.util.Log
import android.util.Size
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import science.credo.mobiledetector2.Config
import java.lang.IllegalArgumentException
import java.util.*

class ConfigurationFinder(
        val context: Context,
        val configurationCallback: ConfigurationCallback) {

    companion object {
        public const val DEMANDED_EXPOSURE_TIME = 3000000000L
        public const val STEP = 250000000L
        public const val MIN_DIMENSION = 720
    }

    interface TemplateTestCallback {
        fun onTempleteResult(template: Int)
    }


    data class ConfigurationToCheck(
            val format: Int,
            val template: Int,
            val size: Size,
            val exposureTime: Long
    )


    public fun start() {
        GlobalScope.launch {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                val queue = getPossibleConfigurations()
                checkConfiguration(queue)

            } else {

            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun checkConfiguration(queue: Queue<ConfigurationToCheck>) {
        val conf = queue.poll()
        if (conf != null) {
            val configuration2Util = Camera2ConfigurationUtil2(conf, object : ConfigurationCallback {
                override fun onConfigurationLog(warning: String) {
                    println(warning)
                }

                override fun onConfigurationSuccess(configuration: DeviceConfiguration) {
                    println("========== configuration test")
                    configurationCallback.onConfigurationSuccess(configuration)
                }

                override fun onConfigurationError(error: String?) {
                    println("=================conf error   $error")
                    Handler().postDelayed({
                        checkConfiguration(queue)
                    }, 1000)
                }
            })
            println("=================start next conf: ${conf}")
            configuration2Util.start(context)
        } else {
            configurationCallback.onConfigurationError("CANT FIND ANY CONFIGURATION")
        }


    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private suspend fun getPossibleConfigurations(): Queue<ConfigurationToCheck> {
        val configurationQueue = LinkedList<ConfigurationToCheck>()

        val cameraManager: CameraManager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val cameraId = cameraManager.cameraIdList[Config.CAMERA_ID]
        val characteristics = cameraManager.getCameraCharacteristics(cameraId)


        val capabilities = characteristics.get(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES)
        val confMap = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
        if (capabilities == null || confMap == null) {
            throw IllegalArgumentException()
        }

        val supportedFormats = confMap.outputFormats

        val demandedFormats = intArrayOf(
                ImageFormat.RAW_SENSOR,
                ImageFormat.YUV_420_888,
                ImageFormat.JPEG
        )

        val finalFormats = demandedFormats.intersect(supportedFormats.asIterable()).toIntArray()

        val template = testTemplate()


        val templates = arrayOf(
                CameraDevice.TEMPLATE_MANUAL,
                CameraDevice.TEMPLATE_PREVIEW
        )



        for (format in finalFormats) {
            val sizes = getAvailableOutpusSizes(capabilities, confMap, format)
            for (size in sizes) {
                if (size.height < MIN_DIMENSION || size.width < MIN_DIMENSION) {
                    continue
                }
                for (exposureTime in DEMANDED_EXPOSURE_TIME downTo STEP step STEP) {
                    configurationQueue.add(
                            ConfigurationToCheck(
                                    format,
                                    template,
                                    size,
                                    exposureTime
                            )
                    )
                }
                val littleStep = (STEP / 5)
                for (exposureTime in STEP - littleStep downTo littleStep step littleStep) {
                    configurationQueue.add(
                            ConfigurationToCheck(
                                    format,
                                    template,
                                    size,
                                    exposureTime
                            )
                    )
                }
                configurationQueue.add(
                        ConfigurationToCheck(
                                format,
                                template,
                                size,
                                33000000L
                        )
                )
            }
        }
        return configurationQueue
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getAvailableOutpusSizes(
            capabilities: IntArray,
            confMap: StreamConfigurationMap,
            format: Int): Array<Size> {
        return if (capabilities.contains(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_BURST_CAPTURE)
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val a1 = confMap.getHighResolutionOutputSizes(format)
            val a2 = confMap.getOutputSizes(format)
            (a1 + a2).distinct().toTypedArray()
        } else {
            confMap.getOutputSizes(format)
        }
    }


    @SuppressLint("MissingPermission")
    private suspend fun testTemplate(): Int {

        val mBackgroundThread = HandlerThread("CameraBackground")
        mBackgroundThread.start()
        val mBackgroundHandler = Handler(mBackgroundThread.looper)

        var job: Job? = null

        var template = CameraDevice.TEMPLATE_MANUAL

        val cameraCallback = object : CameraDevice.StateCallback() {
            override fun onDisconnected(p0: CameraDevice) {

            }

            override fun onError(p0: CameraDevice, p1: Int) {
                template = CameraDevice.TEMPLATE_PREVIEW
                job?.cancel()
            }

            override fun onOpened(p0: CameraDevice) {
                try {
                    p0.createCaptureRequest(template)
                } catch (ex: IllegalArgumentException) {
                    template = CameraDevice.TEMPLATE_PREVIEW
                } finally {
                    job?.cancel()
                    p0.close()
                }
            }
        }

        job = GlobalScope.launch {

            val cameraManager: CameraManager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
            cameraManager.openCamera(cameraManager.cameraIdList[Config.CAMERA_ID], cameraCallback, mBackgroundHandler)
            while (true) {
                delay(1000)
            }

        }
        job.join()
        return template
    }


}