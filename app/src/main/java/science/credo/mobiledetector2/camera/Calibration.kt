package science.credo.mobiledetector2.camera

object Calibration {
    val AMPLIFIER = 1.4

    var detectionThreshold = 35
    var noiseThreshold = 10
    var calibrationNoise = 0

}