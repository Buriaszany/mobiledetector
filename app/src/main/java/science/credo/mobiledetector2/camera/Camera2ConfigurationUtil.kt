package science.credo.mobiledetector2.camera

import android.content.Context
import android.graphics.ImageFormat
import android.hardware.camera2.*
import android.media.ImageReader
import android.os.Build
import android.os.Handler
import android.support.annotation.RequiresApi
import android.util.Log
import android.util.Size
import android.view.Surface
import kotlinx.coroutines.*
import science.credo.mobiledetector2.Config
import science.credo.mobiledetector2.resolvers.ConfigurationResolver
import science.credo.mobiledetector2.resolvers.DetectionResolver
import science.credo.mobiledetector2.resolvers.FrameResult
import java.lang.StringBuilder
import java.lang.UnsupportedOperationException
import java.util.*
import java.lang.Exception
import kotlin.collections.ArrayList


//@RequiresApi(api = 21)
//open class Camera2ConfigurationUtil(val configurationCallback: ConfigurationCallback) : Camera2Util() {
//
//
//    override fun start(context: Context) {
//        configurationAttempt(context)
//    }
//
//    //    private var configurationCallback: Camera2ConfigurationUtil.ConfigurationCallback? = null
//    private var imageFormat: Int = ImageFormat.RAW_SENSOR
//
//
//    object DeviceCapabilities {
//        var rawFormatSupported = false
//        var manualSensorSupported = false
//        var aeOffSupported = false
//        var general3AModeOffSupported = false
//        var hardwareLevelFullSupported = false
//    }
//
//
////    object ConfigurationProgress {
////        enum class Stage {
////            NONE,
////            EXPOSURE_TEST,
////            CALCULATION_TIME_TEST
////        }
////
////        var lastTemplate: Int = CameraDevice.TEMPLATE_MANUAL
////        var stage = Stage.NONE
////        var counter = 0
////        var startTimestamp = 0L
////
////        fun clearCounters() {
////            counter = 0
////        }
////
////        fun start() {
////            stage = Stage.EXPOSURE_TEST
////        }
////
////        fun resetTimeStamp() {
////            startTimestamp = System.nanoTime()
////        }
////
////        const val EXPOSURE_TEST_LENGTH = 10
////    }
////
////    object Exposure {
////        const val TOP_EXPOSURE_TIME = 5000000000L
////        private const val ONE_SECOND = 1000000000L
////        const val MARGE_FACTOR = 0.2
////        var timeUnit = ONE_SECOND
////        var maxFrameDuration: Long? = 0L
////        var frameDurationMultiplier = TOP_EXPOSURE_TIME / timeUnit
////        var lastFrameTimestamp: Long? = null
////        var totalCalculationTime = 0L
////        var avgTimeList = ArrayList<Long>()
////
////        fun demandedExposureTime(): Long {
////            return timeUnit * frameDurationMultiplier
////        }
////
////        fun decreaseDurationMultiplier() {
////            --frameDurationMultiplier
////        }
////
////        fun addAvgTime(avgTime: Long) {
////            avgTimeList.add(avgTime)
////        }
////
////        fun getMaxAvgTime(): Long {
////            var maxAvg = 0L
////            for (avg in avgTimeList) {
////                if (avg > maxAvg) {
////                    maxAvg = avg
////                }
////            }
////            return maxAvg
////        }
////    }
//
//
//    private fun checkDevice(context: Context): Pair<Boolean, String?> {
//        var isDeviceOk = true
//
//        val warningStringBuilder = StringBuilder()
//        val cameraManager: CameraManager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
//        val cameraId = cameraManager.cameraIdList[Config.CAMERA_ID]
//        val characteristics = cameraManager.getCameraCharacteristics(cameraId)
//
//
////        val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
////        characteristics.get(SENSOR_INFO_PIXEL_ARRAY_SIZE)
////        characteristics.get(CameraCharacteristics.SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE)
//
//        val aeModes = characteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES)
//        var modes: IntArray? = null
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            modes = characteristics.get(CameraCharacteristics.CONTROL_AVAILABLE_MODES)
//        }
//        val hardwareLevel = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL)
//        val capabilities = characteristics.get(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES)
//
//
//
//        if (hardwareLevel == CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_FULL) {
//            DeviceCapabilities.hardwareLevelFullSupported = true
//        } else if (hardwareLevel == CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY) {
//            isDeviceOk = false
//        } else {
//            warningStringBuilder.append("WARNING! Hardware level full not supported!\nSupported Hardware level for that device: $hardwareLevel\n\n")
//        }
//        if (aeModes != null && aeModes.contains(CameraMetadata.CONTROL_AE_MODE_OFF)) {
//            DeviceCapabilities.aeOffSupported = true
//        } else {
//            warningStringBuilder.append("WARNING! AE off mode not supported!\n\n")
//        }
//        if (modes != null && modes.contains(CameraMetadata.CONTROL_MODE_OFF)) {
//            DeviceCapabilities.general3AModeOffSupported = true
//        } else {
//            warningStringBuilder.append("WARNING! 3A off mode not supported!\n\n")
//        }
//        if (capabilities != null && capabilities.contains(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_RAW)) {
//            DeviceCapabilities.rawFormatSupported = true
//        } else {
//            warningStringBuilder.append("WARNING! RAW format not supported!\n\n")
//        }
//        if (capabilities != null && capabilities.contains(CameraMetadata.REQUEST_AVAILABLE_CAPABILITIES_MANUAL_SENSOR)) {
//            DeviceCapabilities.manualSensorSupported = true
//        } else {
//            warningStringBuilder.append("WARNING! Manual sensor control not supported!\n\n")
//        }
//        if (!DeviceCapabilities.rawFormatSupported || !DeviceCapabilities.manualSensorSupported) {
//            warningStringBuilder.append("Supported capabilities ${capabilities.toList()}\n\n")
//        }
//
//        val warnings = warningStringBuilder.toString()
//        warningStringBuilder.clear()
//        return if (!DeviceCapabilities.hardwareLevelFullSupported ||
//                !DeviceCapabilities.aeOffSupported ||
//                !DeviceCapabilities.general3AModeOffSupported ||
//                !DeviceCapabilities.rawFormatSupported ||
//                !DeviceCapabilities.manualSensorSupported) {
//            Pair<Boolean, String?>(isDeviceOk, warnings)
//        } else {
//            Pair<Boolean, String?>(isDeviceOk, null)
//        }
//    }
//
//    private fun configurationAttempt(context: Context) {
//        configurationAttempt(context, ImageFormat.RAW_SENSOR)
//    }
//
//
//    private fun configurationAttempt(context: Context, imageFormat: Int) {
//
//        this.context = context
//        this.imageFormat = imageFormat
//
//        startBackgroundThread()
//
//        val cameraManager: CameraManager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
//        val cameraId = cameraManager.cameraIdList[Config.CAMERA_ID]
//        val characteristics = cameraManager.getCameraCharacteristics(cameraId)
//
//        val confMap = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
//        confMap.getOutputSizes(imageFormat)
//
//        val sizes = confMap.getOutputSizes(imageFormat)
//        if (sizes == null) {
//            if (imageFormat == ImageFormat.RAW_SENSOR) {
//                rerunConfiguration(context, ImageFormat.YUV_420_888)
//            } else {
//                configurationCallback.onConfigurationError("pthu")
//            }
//            return
//        }
//        val mVideoSize = sizes[0]
//
//
////        val mVideoSize = characteristics.get(SENSOR_INFO_PIXEL_ARRAY_SIZE)
//
//        val exposureRanges = characteristics.get(CameraCharacteristics.SENSOR_INFO_EXPOSURE_TIME_RANGE)
//        val lowerIsoValue = characteristics.get(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE)?.lower
//                ?: 0
//        if (Exposure.maxFrameDuration == 0L) {
//            Exposure.maxFrameDuration = characteristics.get(CameraCharacteristics.SENSOR_INFO_MAX_FRAME_DURATION)
//        }
//
//        println("==========configurationAttempt   max frameDuration  ${Exposure.maxFrameDuration}")
//
//        try {
//            cameraManager.openCamera(cameraId, object : CameraDevice.StateCallback() {
//                override fun onError(p0: CameraDevice, p1: Int) {
//                    if (ConfigurationProgress.stage == ConfigurationProgress.Stage.EXPOSURE_TEST) {
//                        configurationCallback.onConfigurationLog("\n\nERROR! EXPOSURE TIME to high \n")
//                        configurationCallback.onConfigurationLog("Trying one more time with lower EXPOSURE TIME \n")
//
//                        Exposure.decreaseDurationMultiplier()
//                        rerunConfiguration(context, imageFormat)
//                    } else {
//                        configurationCallback.onConfigurationError("ERROR! Can't open camera!");
//                    }
//
//                }
//
//                override fun onOpened(cameraDevice: CameraDevice) {
//
//
//                    configurationCallback.onConfigurationLog("Camera opened successfully\n")
//                    cam = cameraDevice
//
//                    configurationCallback.onConfigurationLog("Setting TEMPLATE_MANUAL\n")
//                    try {
//                        captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_MANUAL)
//                        if (captureBuilder.get(CaptureRequest.CONTROL_AE_MODE) == CameraMetadata.CONTROL_AE_MODE_OFF) {
//                            configurationCallback.onConfigurationLog("TEMPLATE_MANUAL set successfully\n")
//                        } else {
//                            configurationCallback.onConfigurationLog("WARNING !! TEMPLATE_MANUAL set failed\n")
//                        }
//                    } catch (e: IllegalArgumentException) {
//                        e.printStackTrace()
//                        configurationCallback.onConfigurationLog("WARNING !! TEMPLATE_MANUAL set failed\n")
//                        configurationCallback.onConfigurationLog("Setting TEMPLATE_PREVIEW\n")
//                        captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
//                        configurationCallback.onConfigurationLog("TEMPLATE_PREVIEW set successfully\n")
//                        ConfigurationProgress.lastTemplate = CameraDevice.TEMPLATE_PREVIEW
//                    }
//
//
//
//
//                    configurationCallback.onConfigurationLog("Setting CONTROL_AE_MODE mode to CONTROL_AE_MODE_OFF\n")
//                    captureBuilder.set<Int>(CaptureRequest.CONTROL_AE_MODE, CameraMetadata.CONTROL_AE_MODE_OFF)
//                    if (captureBuilder.get(CaptureRequest.CONTROL_AE_MODE) == CameraMetadata.CONTROL_AE_MODE_OFF) {
//                        configurationCallback.onConfigurationLog("CONTROL_AE_MODE_OFF set successfully\n")
//                    } else {
//                        configurationCallback.onConfigurationLog("WARNING !! CONTROL_AE_MODE_OFF set failed\n")
//                    }
//
//                    configurationCallback.onConfigurationLog("Setting CONTROL_MODE mode to CONTROL_MODE_OFF\n")
//                    captureBuilder.set<Int>(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_OFF)
//                    if (captureBuilder.get(CaptureRequest.CONTROL_MODE) == CameraMetadata.CONTROL_MODE_OFF) {
//                        configurationCallback.onConfigurationLog("CONTROL_MODE_OFF set successfully\n")
//                    } else {
//                        configurationCallback.onConfigurationLog("WARNING !! CONTROL_MODE_OFF set failed\n")
//                    }
//
//                    configurationCallback.onConfigurationLog("Setting SENSOR_SENSITIVITY mode to lower value\n")
//                    captureBuilder.set<Int>(CaptureRequest.SENSOR_SENSITIVITY, lowerIsoValue)
//                    if (captureBuilder.get(CaptureRequest.SENSOR_SENSITIVITY) == lowerIsoValue) {
//                        configurationCallback.onConfigurationLog("SENSOR_SENSITIVITY set successfully\n")
//                    } else {
//                        configurationCallback.onConfigurationLog("WARNING !! SENSOR_SENSITIVITY set failed\n")
//                    }
//
//
//
//                    if (Exposure.maxFrameDuration != null) {
//                        if (Exposure.maxFrameDuration!! > Exposure.demandedExposureTime()) {
//                            configurationCallback.onConfigurationLog("Setting SENSOR_FRAME_DURATION mode to max: ${Exposure.demandedExposureTime()}\n")
//                            captureBuilder.set(CaptureRequest.SENSOR_FRAME_DURATION, Exposure.demandedExposureTime())
//                            configurationCallback.onConfigurationLog("Setting SENSOR_EXPOSURE_TIME to ${Exposure.demandedExposureTime()}\n")
//                            captureBuilder.set<Long>(CaptureRequest.SENSOR_EXPOSURE_TIME, Exposure.demandedExposureTime())
//                        } else {
//                            if (ConfigurationProgress.stage == ConfigurationProgress.Stage.NONE) {
//                                Exposure.frameDurationMultiplier = Exposure.TOP_EXPOSURE_TIME / Exposure.maxFrameDuration!!
//                                Exposure.timeUnit = Exposure.maxFrameDuration!!
//                            }
//                            configurationCallback.onConfigurationLog("Setting SENSOR_FRAME_DURATION mode to max: ${Exposure.maxFrameDuration!!}\n")
//                            captureBuilder.set(CaptureRequest.SENSOR_FRAME_DURATION, Exposure.demandedExposureTime())
//                            configurationCallback.onConfigurationLog("Setting SENSOR_EXPOSURE_TIME to ${Exposure.demandedExposureTime()}\n")
//                            captureBuilder.set<Long>(CaptureRequest.SENSOR_EXPOSURE_TIME, Exposure.demandedExposureTime())
//                        }
//                    }
//
//
//                    mImageReader = ImageReader.newInstance(mVideoSize!!.width,
//                            mVideoSize!!.height,
//                            imageFormat, 2)
//
//
//                    mImageReader!!.setOnImageAvailableListener(this@Camera2ConfigurationUtil, getBackgroundHandler())
//
//
//                    val surfaces = ArrayList<Surface>(1)
//                    val readerSurface = mImageReader!!.surface
//                    surfaces.add(readerSurface)
//                    captureBuilder!!.addTarget(readerSurface)
//
//                    configurationCallback.onConfigurationLog("Creating capture session\n")
//                    try {
//                        cameraDevice.createCaptureSession(surfaces,
//                                object : CameraCaptureSession.StateCallback() {
//
//                                    override fun onConfigured(session: CameraCaptureSession) {
//                                        configurationCallback.onConfigurationLog("Capture session configured successfully\n")
//                                        configurationCallback.onConfigurationLog("ImageFormat set to ${imageFormat
//                                                ?: ImageFormat.RAW_SENSOR}\n")
//
//                                        startPreview(session)
//                                    }
//
//                                    override fun onConfigureFailed(session: CameraCaptureSession) {
//
//                                        configurationCallback.onConfigurationLog("ERROR ! Capture session configured failed\n")
//                                        configurationCallback.onConfigurationError("");
//
//
//                                    }
//                                }, getBackgroundHandler())
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                        if (imageFormat == ImageFormat.RAW_SENSOR) {
//                            rerunConfiguration(context, ImageFormat.YUV_420_888)
//                        } else {
//                            configurationCallback.onConfigurationError("pthu")
//                        }
//                    }
//
//
//                }
//
//                override fun onDisconnected(p0: CameraDevice) {
//
//                    println("==========onCaptureSequenceAborted   $p0")
//
//
//                }
//
//            }, getBackgroundHandler())
//        } catch (e: SecurityException) {
//            e.printStackTrace()
//        }
//    }
//
//
//    override fun startPreview(session: CameraCaptureSession) {
//
//        if (ConfigurationProgress.stage == ConfigurationProgress.Stage.NONE) {
//            ConfigurationProgress.start()
//        }
//        ConfigurationProgress.resetTimeStamp()
//
//        this.session = session
//        session.setRepeatingRequest(captureBuilder.build(), object : CameraCaptureSession.CaptureCallback() {
//
//
//            override fun onCaptureCompleted(session: CameraCaptureSession, request: CaptureRequest, result: TotalCaptureResult) {
//
//
////                val exposureTime = result.get(CaptureResult.SENSOR_EXPOSURE_TIME)
////
////                if (exposureTime != null) {
////                    val marge = Exposure.demandedExposureTime() * Exposure.MARGE_FACTOR
////                    if (exposureTime < Exposure.demandedExposureTime() - marge ||
////                            exposureTime > Exposure.demandedExposureTime() + marge) {
////
////                        configurationCallback!!.onConfigurationLog("\n\nERROR! EXPOSURE TIME set failed \n")
////                        configurationCallback!!.onConfigurationLog("Trying one more time with lower EXPOSURE TIME \n")
////
////                        Exposure.decreaseDurationMultiplier()
////                        rerunConfiguration(context!!, imageFormat)
////
////                    }
////                }
////
////
//////                else if (frameDuration != null) {
//////                    val dE = exposureTime * 0.05
//////                    if (frameDuration < exposureTime - dE || frameDuration > exposureTime + dE) {
//////                        Log.i("LOG", "============ FrameDuration dont match $frameDuration")
//////                        Exposure.decreaseDurationMultiplier()
//////                        rerunConfiguration(context!!, configurationCallback!!, imageFormat)
//////                    }
//////
//////                }
////
////                super.onCaptureCompleted(session, request, result)
//            }
//
//
//        }, getBackgroundHandler())
//
//    }
//
//    fun rerunConfiguration(context: Context, imageFormat: Int) {
//
//        stop()
//        Handler().postDelayed(Runnable {
//            configurationAttempt(context, imageFormat)
//        }, 3000)
//
//    }
//
//    var recenltySaved = false;
//
//    override fun onImageAvailable(p0: ImageReader?) {
//
//        try {
//            val image = p0!!.acquireLatestImage()
//            var width = 0
//            var height = 0
//            var imageFormat: Int
//            if (image != null) {
//                width = image.width
//                height = image.height
//                imageFormat = p0.imageFormat
//                val buffer1 = image.planes[0].buffer
//                var data1: ByteArray? = null
//                data1 = ByteArray(buffer1.remaining())
//                buffer1.get(data1)
//                Log.i("LOG", "===================????? $data1")
//                if (ConfigurationProgress.stage == ConfigurationProgress.Stage.EXPOSURE_TEST) {
//                    Log.i("LOG", "===================EXPOSURE_TEST $data1")
//                    if (ConfigurationProgress.counter == ConfigurationProgress.EXPOSURE_TEST_LENGTH) {
//
//                        val avgFrameDuration = (System.nanoTime() - ConfigurationProgress.startTimestamp) / ConfigurationProgress.counter
//                        val marge = Exposure.demandedExposureTime() * Exposure.MARGE_FACTOR
//
//                        Log.i("LOG", "===================EXPOSURE_TEST avg $avgFrameDuration  ----  ${Exposure.demandedExposureTime()}")
//
//                        if (avgFrameDuration < Exposure.demandedExposureTime() - marge ||
//                                avgFrameDuration > Exposure.demandedExposureTime() + marge) {
//
//                            if (Exposure.frameDurationMultiplier > 1) {
//                                Exposure.addAvgTime(avgFrameDuration)
//                                Exposure.decreaseDurationMultiplier()
//                                rerunConfiguration(context!!, imageFormat)
//                            } else {
//                                Log.i("LOG", "===================EXPOSURE_TEST final avg ${Exposure.getMaxAvgTime()}")
//                                ConfigurationProgress.stage = ConfigurationProgress.Stage.CALCULATION_TIME_TEST
//                                ConfigurationProgress.clearCounters()
//                            }
//
//                        } else {
//                            ConfigurationProgress.stage = ConfigurationProgress.Stage.CALCULATION_TIME_TEST
//                            ConfigurationProgress.clearCounters()
//                        }
//                        Log.i("LOG", "===================avg frame duration $avgFrameDuration")
//                    }
//                    ConfigurationProgress.counter++
//                } else if (ConfigurationProgress.stage == ConfigurationProgress.Stage.CALCULATION_TIME_TEST) {
//                    GlobalScope.launch {
//                        ConfigurationProgress.counter++
//                        val pixelPrecision = if (imageFormat == ImageFormat.RAW_SENSOR) 2 else 1
//                        Log.i("LOG", "===================sizes ${data1.size}   $width  $height")
//
//                        val scaledSize = if (imageFormat == ImageFormat.RAW_SENSOR) {
//                            getScaledSizeForRaw(width, height)
//                        } else {
//                            Size(width, height)
//                        }
//
//                        val deviceConfiguration = DeviceConfiguration(
//                                width,
//                                height,
//                                scaledSize.width,
//                                scaledSize.height,
//                                ConfigurationProgress.lastTemplate,
//                                imageFormat,
//                                captureBuilder.get(CaptureRequest.SENSOR_SENSITIVITY)!!,
//                                Exposure.demandedExposureTime(),
//                                Exposure.demandedExposureTime(),
//                                captureBuilder.get(CaptureRequest.CONTROL_MODE)!!,
//                                captureBuilder.get(CaptureRequest.CONTROL_AE_MODE)!!
//                        )
//
//                        val frame = Frame(data1, width, height, imageFormat, System.currentTimeMillis())
//                        val frameResult = FrameResult(ConfigurationResolver())
//
//
//                        val time = System.nanoTime()
//                        frameResult.resolve(frame, deviceConfiguration)
//                        val calcTime = System.nanoTime() - time
//                        Exposure.totalCalculationTime += calcTime
//                        Log.i("LOG", "===================CALCULATION_TIME_TEST ${ConfigurationProgress.counter}:   $calcTime   isOK? ${calcTime < Exposure.demandedExposureTime()}")
//
//                        if (ConfigurationProgress.counter == ConfigurationProgress.EXPOSURE_TEST_LENGTH) {
//                            val avgCalculationTime = Exposure.totalCalculationTime / ConfigurationProgress.EXPOSURE_TEST_LENGTH
//                            Log.i("LOG", "===================CALCULATION_TIME_TEST  result:   ${avgCalculationTime}  ---  ${Exposure.demandedExposureTime()}")
//                            if (avgCalculationTime < Exposure.demandedExposureTime()) {
//                                stop()
//
//                                configurationCallback.onConfigurationSuccess(deviceConfiguration)
//                            } else {
//                                stop()
//                                configurationCallback.onConfigurationError("dudududupa")
//                            }
//                        }
//
//                    }
//
//
//                }
//
////                calculationTime = System.nanoTime()
////                val (max, maxindex, sum, blacks, highs, blacksafterreduction) = calcHistogram(data1, width, height, 40, 0);
////                Log.i("LOG", "===================blacks&hi $blacks    $highs")
////                if (highs > 900 && !recenltySaved) {
////                    recenltySaved = true
////                    save(data1, width, height)
////                }
////
////                totalCalculationTime += (System.nanoTime() - calculationTime)
////                Log.i("LOG", "===================calculation time " + (System.nanoTime() - calculationTime))
////                calculationCounter++
//
//                image.close()
//
//
//            }
//
////            if (counter == 10L) {
////                p0.close()
////                val countedExposureTime = totalExposureTime / totalExposureCounter;
////                val countedCalculationTime = totalCalculationTime / calculationCounter;
////
////                configurationCallback?.onConfigurationLog("Exposure time : $countedExposureTime\n")
////                configurationCallback?.onConfigurationLog("Average calculationTime : $countedCalculationTime\n")
////                Log.i("LOG", "=============  $countedCalculationTime  $countedExposureTime    ${countedCalculationTime < countedExposureTime}")
////                if (countedCalculationTime < countedExposureTime || true) {
////                    configurationCallback?.onConfigurationLog("Configuration created!")
////                    clearCounters()
////                    configurationCallback?.onConfigurationSuccess(DeviceConfiguration(
////                            width,
////                            height,
////                            CameraDevice.TEMPLATE_MANUAL,
////                            imageFormat,
////                            captureBuilder.get(CaptureRequest.SENSOR_EXPOSURE_TIME)!!,
////                            captureBuilder.get(CaptureRequest.CONTROL_MODE)!!,
////                            captureBuilder.get(CaptureRequest.CONTROL_AE_MODE)!!)
////                    )
////                } else {
////                    TODO("try to increase exposure time, and if not possible, set image format to YUV and decrease frame size")
////                }
////                stop()
////
////            }
//
//        } catch (e: UnsupportedOperationException) {
//            e.printStackTrace()
//            configurationCallback.onConfigurationLog("WARNING !! image format ${p0?.imageFormat} not supported\n\n")
//            ConfigurationProgress.clearCounters()
//            stop()
//            if (p0?.imageFormat == ImageFormat.RAW_SENSOR) {
//                rerunConfiguration(context!!, ImageFormat.YUV_420_888)
//            }
//            p0?.close()
//        }
//
//    }
//
//    private fun getScaledSizeForRaw(width: Int, height: Int): Size {
//
//        val scaledWidth = when {
//            width % 4 == 0 -> width / 4
//            width % 3 == 0 -> width / 3
//            else -> width / 2
//        }
//
//        val scaledHeight = when {
//            height % 4 == 0 -> height / 4
//            height % 3 == 0 -> height / 3
//            else -> height / 2
//        }
//
//        return Size(scaledWidth, scaledHeight)
//
//    }
//
//
//    override fun stop() {
//        super.stop()
//        Exposure.lastFrameTimestamp = null
//        ConfigurationProgress.clearCounters()
//    }
//
//}
