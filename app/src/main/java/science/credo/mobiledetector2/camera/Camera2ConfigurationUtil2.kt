package science.credo.mobiledetector2.camera

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageFormat
import android.hardware.camera2.*
import android.media.ImageReader
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.Log
import android.util.Size
import android.view.Surface
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import science.credo.mobiledetector2.Config
import science.credo.mobiledetector2.resolvers.ConfigurationResolver
import science.credo.mobiledetector2.resolvers.FrameResult
import science.credo.mobiledetector2.resolvers.Metadata
import science.credo.mobiledetector2.toPositiveInt
import java.lang.Exception
import java.lang.UnsupportedOperationException

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class Camera2ConfigurationUtil2(
        val configurationToCheck: ConfigurationFinder.ConfigurationToCheck,
        val configurationCallback: ConfigurationCallback
) : Camera2Util() {


    object ConfigurationProgress {
        const val MARGE_FACTOR = 0.1

        enum class Stage {
            NONE,
            EXPOSURE_TEST,
            CALCULATION_TIME_TEST
        }


        var stage = Stage.NONE
        var counter = 0
        var startTimestamp = 0L
        var totalCalculationTime = 0L

        fun clearCounters() {
            counter = 0
        }

        fun start() {
            startTimestamp = System.nanoTime()
            Log.i("LOG","========== reset time " + startTimestamp)
            stage = Stage.EXPOSURE_TEST
        }


        const val EXPOSURE_TEST_LENGTH = 10
    }


    override fun start(context: Context) {

        configurationAttempt(context)

    }


    private fun configurationAttempt(context: Context) {

        this.context = context
        ConfigurationProgress.stage = ConfigurationProgress.Stage.NONE
        startBackgroundThread()

        val cameraManager: CameraManager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val cameraId = cameraManager.cameraIdList[Config.CAMERA_ID]
        val characteristics = cameraManager.getCameraCharacteristics(cameraId)

        val lowerIsoValue = characteristics.get(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE)?.lower
                ?: 0

        try {
            cameraManager.openCamera(cameraId, object : CameraDevice.StateCallback() {
                override fun onError(p0: CameraDevice, p1: Int) {
                    configurationCallback.onConfigurationError(null)

                }

                override fun onOpened(cameraDevice: CameraDevice) {


                    cam = cameraDevice
                    captureBuilder = cameraDevice.createCaptureRequest(configurationToCheck.template)



                    configurationCallback.onConfigurationLog("Setting CONTROL_AE_MODE mode to CONTROL_AE_MODE_OFF\n")
                    captureBuilder.set<Int>(CaptureRequest.CONTROL_AE_MODE, CameraMetadata.CONTROL_AE_MODE_OFF)
                    if (captureBuilder.get(CaptureRequest.CONTROL_AE_MODE) == CameraMetadata.CONTROL_AE_MODE_OFF) {
                        configurationCallback.onConfigurationLog("CONTROL_AE_MODE_OFF set successfully\n")
                    } else {
                        configurationCallback.onConfigurationLog("WARNING !! CONTROL_AE_MODE_OFF set failed\n")
                    }

                    configurationCallback.onConfigurationLog("Setting CONTROL_MODE mode to CONTROL_MODE_OFF\n")
                    captureBuilder.set<Int>(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_OFF)
                    if (captureBuilder.get(CaptureRequest.CONTROL_MODE) == CameraMetadata.CONTROL_MODE_OFF) {
                        configurationCallback.onConfigurationLog("CONTROL_MODE_OFF set successfully\n")
                    } else {
                        configurationCallback.onConfigurationLog("WARNING !! CONTROL_MODE_OFF set failed\n")
                    }

                    configurationCallback.onConfigurationLog("Setting SENSOR_SENSITIVITY mode to lower value\n")
                    captureBuilder.set<Int>(CaptureRequest.SENSOR_SENSITIVITY, lowerIsoValue)
                    if (captureBuilder.get(CaptureRequest.SENSOR_SENSITIVITY) == lowerIsoValue) {
                        configurationCallback.onConfigurationLog("SENSOR_SENSITIVITY set successfully\n")
                    } else {
                        configurationCallback.onConfigurationLog("WARNING !! SENSOR_SENSITIVITY set failed\n")
                    }

                    configurationCallback.onConfigurationLog("Setting SENSOR_FRAME_DURATION to: ${configurationToCheck.exposureTime}\n")

                    captureBuilder.set<Long>(CaptureRequest.SENSOR_EXPOSURE_TIME, configurationToCheck.exposureTime)

                    configurationCallback.onConfigurationLog("Setting SENSOR_EXPOSURE_TIME to ${configurationToCheck.exposureTime}\n")

                    captureBuilder.set(CaptureRequest.SENSOR_FRAME_DURATION, configurationToCheck.exposureTime)


                    mImageReader = ImageReader.newInstance(
                            configurationToCheck.size.width,
                            configurationToCheck.size.height,
                            configurationToCheck.format,
                            2)


                    mImageReader!!.setOnImageAvailableListener(this@Camera2ConfigurationUtil2, getBackgroundHandler())


                    val surfaces = ArrayList<Surface>(1)
                    val readerSurface = mImageReader!!.surface
                    surfaces.add(readerSurface)
                    captureBuilder.addTarget(readerSurface)

                    configurationCallback.onConfigurationLog("Creating capture session\n")
                    try {
                        cameraDevice.createCaptureSession(surfaces,
                                object : CameraCaptureSession.StateCallback() {

                                    override fun onConfigured(session: CameraCaptureSession) {
                                        configurationCallback.onConfigurationLog("Capture session configured successfully\n")
                                        configurationCallback.onConfigurationLog("ImageFormat set to ${configurationToCheck.format}\n")
                                        startPreview(session)
                                    }

                                    override fun onConfigureFailed(session: CameraCaptureSession) {
                                        stop()
                                        configurationCallback.onConfigurationLog("ERROR ! Capture session configured failed\n")
                                        configurationCallback.onConfigurationError(null)


                                    }
                                }, getBackgroundHandler())
                    } catch (e: Exception) {
                        e.printStackTrace()
                        stop()
                        configurationCallback.onConfigurationError(null)
                    }


                }

                override fun onDisconnected(p0: CameraDevice) {

                    println("==========onCaptureSequenceAborted   $p0")


                }

            }, getBackgroundHandler())
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    override fun stop() {
        ConfigurationProgress.clearCounters()
        super.stop()
    }

    override fun startPreview(session: CameraCaptureSession) {

        if (ConfigurationProgress.stage == ConfigurationProgress.Stage.NONE) {
            ConfigurationProgress.start()
        }

        session.setRepeatingRequest(
                captureBuilder.build(),
                object : CameraCaptureSession.CaptureCallback() {

                    override fun onCaptureSequenceAborted(session: CameraCaptureSession, sequenceId: Int) {
                        super.onCaptureSequenceAborted(session, sequenceId)
                    }

                    override fun onCaptureCompleted(session: CameraCaptureSession, request: CaptureRequest, result: TotalCaptureResult) {
                        super.onCaptureCompleted(session, request, result)
                    }

                    override fun onCaptureFailed(session: CameraCaptureSession, request: CaptureRequest, failure: CaptureFailure) {
                        super.onCaptureFailed(session, request, failure)
                    }

                    override fun onCaptureSequenceCompleted(session: CameraCaptureSession, sequenceId: Int, frameNumber: Long) {
                        super.onCaptureSequenceCompleted(session, sequenceId, frameNumber)
                    }

                    override fun onCaptureStarted(session: CameraCaptureSession, request: CaptureRequest, timestamp: Long, frameNumber: Long) {
                        super.onCaptureStarted(session, request, timestamp, frameNumber)
                    }

                    override fun onCaptureProgressed(session: CameraCaptureSession, request: CaptureRequest, partialResult: CaptureResult) {
                        super.onCaptureProgressed(session, request, partialResult)
                    }

                    override fun onCaptureBufferLost(session: CameraCaptureSession, request: CaptureRequest, target: Surface, frameNumber: Long) {
                        super.onCaptureBufferLost(session, request, target, frameNumber)
                    }
                },
                getBackgroundHandler()
        )

    }



    override fun onImageAvailable(p0: ImageReader?) {

        try {
            val image = p0!!.acquireLatestImage()
            if (image != null) {

                if (configurationToCheck.size.width != image.width
                        || configurationToCheck.size.height != image.height
                        || configurationToCheck.format != p0.imageFormat) {
                    configurationCallback.onConfigurationError(null)
                }

                val buffer1 = image.planes[0].buffer
                var data1: ByteArray? = null
                data1 = ByteArray(buffer1.remaining())
                buffer1.get(data1)
                Log.i("LOG", "===================progress ${ConfigurationProgress.counter}   ")
                if (ConfigurationProgress.stage == ConfigurationProgress.Stage.EXPOSURE_TEST) {
                    if (ConfigurationProgress.counter == ConfigurationProgress.EXPOSURE_TEST_LENGTH) {

                        val avgFrameDuration = (System.nanoTime() - ConfigurationProgress.startTimestamp) / ConfigurationProgress.counter
                        val marge = configurationToCheck.exposureTime * ConfigurationProgress.MARGE_FACTOR

                        Log.i("LOG", "===================avg frame duration $avgFrameDuration  ${configurationToCheck.exposureTime} ")

//                        lastAverageExposureTime = avgFrameDuration

                        if (avgFrameDuration < configurationToCheck.exposureTime - marge) {
                            stop()
                            configurationCallback.onConfigurationError(null)
                        } else {
                            ConfigurationProgress.stage = ConfigurationProgress.Stage.CALCULATION_TIME_TEST
                            ConfigurationProgress.clearCounters()
                        }

                    }
                    ConfigurationProgress.counter++

                } else if (ConfigurationProgress.stage == ConfigurationProgress.Stage.CALCULATION_TIME_TEST) {
                    Log.i("LOG", "===================meh meh")


                    val scaledSize = if (configurationToCheck.format == ImageFormat.RAW_SENSOR) {
                        getScaledSizeForRaw(configurationToCheck.size.width, configurationToCheck.size.height)
                    } else {
                        Size(configurationToCheck.size.width, configurationToCheck.size.height)
                    }

                    val deviceConfiguration = DeviceConfiguration(
                            configurationToCheck.size.width,
                            configurationToCheck.size.height,
                            scaledSize.width,
                            scaledSize.height,
                            configurationToCheck.template,
                            configurationToCheck.format,
                            captureBuilder.get(CaptureRequest.SENSOR_SENSITIVITY)!!,
                            configurationToCheck.exposureTime,
                            configurationToCheck.exposureTime,
                            configurationToCheck.exposureTime,
                            captureBuilder.get(CaptureRequest.CONTROL_MODE)!!,
                            captureBuilder.get(CaptureRequest.CONTROL_AE_MODE)!!
                    )

                    val frame = Frame(
                            data1,
                            configurationToCheck.size.width,
                            configurationToCheck.size.height,
                            configurationToCheck.format,
                            0,
                            System.currentTimeMillis()
                    )
                    val frameResult = FrameResult(ConfigurationResolver())

                    val time = System.nanoTime()
                    calculate(frame, deviceConfiguration)
                    val calcTime = System.nanoTime() - time
                    ConfigurationProgress.totalCalculationTime += calcTime
                    Log.i("LOG", "===================CALCULATION_TIME_TEST ${ConfigurationProgress.counter}:   $calcTime   isOK? ${calcTime < configurationToCheck.exposureTime}")


                    if (ConfigurationProgress.counter == ConfigurationProgress.EXPOSURE_TEST_LENGTH) {
                        val avgCalculationTime = ConfigurationProgress.totalCalculationTime / ConfigurationProgress.EXPOSURE_TEST_LENGTH


                        deviceConfiguration.avgCalculationTime = avgCalculationTime
                        Log.i("LOG", "===================CALCULATION_TIME_TEST  result:   ${avgCalculationTime}  ---  ${configurationToCheck.exposureTime}")
                        if (avgCalculationTime < configurationToCheck.exposureTime) {
                            configurationCallback.onConfigurationSuccess(deviceConfiguration)
                            stop()

                        } else {
                            deviceConfiguration.isLossy =true
                            configurationCallback.onConfigurationSuccess(deviceConfiguration)
                            stop()

                        }
                    }
                    ConfigurationProgress.counter++

                }

                image.close()

            }


        } catch (e: Exception) {
            e.printStackTrace()
            stop()
            configurationCallback.onConfigurationError(null)
        }

    }

    private fun getScaledSizeForRaw(width: Int, height: Int): Size {

        val scaledWidth = when {
            width % 4 == 0 -> width / 4
            width % 3 == 0 -> width / 3
            else -> width / 2
        }

        val scaledHeight = when {
            height % 4 == 0 -> height / 4
            height % 3 == 0 -> height / 3
            else -> height / 2
        }

        return Size(scaledWidth, scaledHeight)

    }


    private fun calculate(
            frame: Frame,
            configuration: DeviceConfiguration
    ): Triple<ByteArray?, Bitmap?, Metadata?> {


        val time = System.nanoTime()

        val bitmapArray = IntArray(configuration.scaledWidth * configuration.scaledHeight)
        val pixelPrecision = if (configuration.imageFormat == ImageFormat.RAW_SENSOR) 2 else 1


        val scaleWidth = frame.width / configuration.scaledWidth
        val scaleHeight = frame.height / configuration.scaledHeight
        val scale = scaleWidth * scaleHeight

        for (r in 0 until frame.height) {
            val indexRow = r * frame.width * pixelPrecision
            val scaledIndexRow = r / scaleWidth * configuration.scaledWidth
            var c = 0
            while (c < frame.width * pixelPrecision) {
                val index = indexRow + c
                val newIndex = scaledIndexRow + c / pixelPrecision / scaleHeight
                val byte = frame.byteArray[index].toPositiveInt()
                bitmapArray[newIndex] = bitmapArray[newIndex] + byte
                c += pixelPrecision
            }
        }

        var sum = 0L
        var min = 255
        var max = 0
        var maxIndex = 0
        for (i in 0 until bitmapArray.size) {

            val avg = bitmapArray[i] / scale
            sum += avg

            val red = avg shl 16 and 0x00FF0000
            val green = avg shl 8 and 0x0000FF00
            val blue = avg and 0x000000FF
            val result: Int = red or green or blue
            bitmapArray[i] = result

            if (avg < min) {
                min = avg
            }
            if (avg > max) {
                max = avg
                maxIndex = i
            }
        }

        val noiseAvg = (sum / bitmapArray.size).toInt()
        val x = maxIndex % configuration.scaledWidth
        val y = maxIndex / configuration.scaledWidth

        val metadata = Metadata(
                x,
                y,
                noiseAvg,
                max,
                maxIndex,
                configuration.scaledWidth,
                configuration.scaledHeight)


        return Triple(null, null, metadata)

    }


}