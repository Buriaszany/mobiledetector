package science.credo.mobiledetector2.camera

import android.content.Context
import android.hardware.camera2.*
import android.media.ImageReader
import android.os.Build
import android.os.SystemClock
import android.support.annotation.RequiresApi
import android.util.Size
import android.view.Surface
import science.credo.mobiledetector2.Config
import science.credo.mobiledetector2.tools.Tools.Companion.getImageFormatName
import java.time.Clock
import java.util.*

//
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class Camera2PostConfiguration(
        private val conf: DeviceConfiguration,
        private val callback: FrameCallback
) : Camera2Util() {

    private val TAG = Camera2PostConfiguration::class.java.simpleName
    var lastFrameTimestamp: Long = System.nanoTime()

    override fun onImageAvailable(p0: ImageReader?) {

        val ts = System.nanoTime()
        val exposure = ts - lastFrameTimestamp

        lastFrameTimestamp = ts
        val image = p0!!.acquireLatestImage()
        val timestamp = System.currentTimeMillis()-((SystemClock.elapsedRealtimeNanos()-image.timestamp)/1000)
//        val c = Calendar.getInstance()
//        c.timeInMillis = timestamp
//        println("==============on Image Available ${c.get(Calendar.HOUR_OF_DAY)}:${c.get(Calendar.MINUTE)}:${c.get(Calendar.SECOND)}.${c.get(Calendar.MILLISECOND)}     ||| $timestamp")

        if (image != null) {
            val imageFormat = p0.imageFormat
            val buffer1 = image.planes[0].buffer
            var data: ByteArray? = null
            data = ByteArray(buffer1.remaining())
            buffer1.get(data)
            val frame = Frame(
                    data,
                    image.width,
                    image.height,
                    imageFormat,
                    exposure,
                    timestamp
            )

            callback.onFrameReceived(frame)
            image.close()
        }
    }


    override fun start(context: Context) {
        println("$TAG ==== IMAGE FORMAT ====> ${getImageFormatName(conf.imageFormat)}")
        println("$TAG ==== width ===> ${conf.width}")
        println("$TAG ==== height ===> ${conf.height}")

        this.context = context

        startBackgroundThread()

        val cameraManager: CameraManager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val cameraId = cameraManager.cameraIdList[Config.CAMERA_ID]

        try {
            cameraManager.openCamera(cameraId, object : CameraDevice.StateCallback() {
                override fun onError(p0: CameraDevice, p1: Int) {

                    println("$TAG   onError:$p1")
                }

                override fun onOpened(cameraDevice: CameraDevice) {


                    cam = cameraDevice
                    captureBuilder = cameraDevice.createCaptureRequest(conf.template)
                    captureBuilder.set<Int>(CaptureRequest.CONTROL_AE_MODE, conf.controlAEMode)

                    captureBuilder.set<Int>(CaptureRequest.CONTROL_MODE, conf.controlMode)
                    captureBuilder.set<Int>(CaptureRequest.SENSOR_SENSITIVITY, conf.lowerIsoValue)
                    captureBuilder.set(CaptureRequest.SENSOR_FRAME_DURATION, conf.frameDuration)
                    captureBuilder.set<Long>(CaptureRequest.SENSOR_EXPOSURE_TIME, conf.sensorExposureTime)


                    mImageReader = ImageReader.newInstance(
                            conf.width,
                            conf.height,
                            conf.imageFormat,
                            conf.maxImages)


                    mImageReader!!.setOnImageAvailableListener(this@Camera2PostConfiguration, getBackgroundHandler())


                    val surfaces = ArrayList<Surface>(1)
                    val readerSurface = mImageReader!!.surface
                    surfaces.add(readerSurface)
                    captureBuilder.addTarget(readerSurface)

                    cameraDevice.createCaptureSession(surfaces,
                            object : CameraCaptureSession.StateCallback() {

                                override fun onConfigured(session: CameraCaptureSession) {

                                    startPreview(session)
                                }

                                override fun onConfigureFailed(session: CameraCaptureSession) {

                                    println("$TAG   onConfigureFailed")

                                }
                            }, getBackgroundHandler())


                }

                override fun onDisconnected(p0: CameraDevice) {

                    println("$TAG   onDisconnected")

                }

            }, getBackgroundHandler())
        } catch (e: SecurityException) {
            e.printStackTrace()
        }

    }


    override fun startPreview(session: CameraCaptureSession) {
        this.session = session
        session.setRepeatingRequest(
                captureBuilder.build(),
                object : CameraCaptureSession.CaptureCallback() {

                    override fun onCaptureStarted(session: CameraCaptureSession, request: CaptureRequest, timestamp: Long, frameNumber: Long) {
                        println("==============capture starded ${System.currentTimeMillis()-((SystemClock.elapsedRealtimeNanos()-timestamp)/1000)} ")
                        super.onCaptureStarted(session, request, timestamp, frameNumber)
                    }

                    override fun onCaptureCompleted(session: CameraCaptureSession, request: CaptureRequest, result: TotalCaptureResult) {
                        println("==============capture completed  ${System.currentTimeMillis()}    $result ")
                        super.onCaptureCompleted(session, request, result)
                    }
                }, getBackgroundHandler()
        )
        lastFrameTimestamp = System.nanoTime()
    }

}