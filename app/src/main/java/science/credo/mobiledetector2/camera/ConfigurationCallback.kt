package science.credo.mobiledetector2.camera

interface ConfigurationCallback {
    fun onConfigurationLog(warning: String)
    fun onConfigurationSuccess(configuration: DeviceConfiguration)
    fun onConfigurationError(error: String?)
}