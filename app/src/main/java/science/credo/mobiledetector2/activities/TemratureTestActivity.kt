package science.credo.mobiledetector2.activities

import android.content.Context
import android.content.Intent
import android.graphics.ImageFormat
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_temperature_test.*
//
//class TemratureTestActivity : AppCompatActivity() {
//
//    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_temperature_test)
//        val configuration = intent.getSerializableExtra(Config.EXTRA_DEVICE_CONFIGURATION)
//                as Camera2ConfigurationUtil.DeviceConfiguration
//
//
////        configuration.imageFormat=ImageFormat.YUV_420_888;
////        configuration.width = 1440
////        configuration.height = 1440
////        configuration.sensorExposureTime = 250000000
////        configuration.frameDuration = 250000000
//
//        val tnc = TemperatureNoiseCheck(
//                configuration,
//                configuration.width,
//                configuration.height,
//                object : TemperatureNoiseCheck.Callback {
//                    override fun onFinished() {
//                        finish()
//                    }
//                }
//        )
//        tnc.start(this)
//
//        btStop.setOnClickListener {
//
//            tnc.stop()
//
//        }
//
//    }
//
//    companion object {
//        fun intent(context: Context, configuration: Camera2ConfigurationUtil.DeviceConfiguration)
//                : Intent {
//            val intent: Intent = Intent(context, TemratureTestActivity::class.java)
//            intent.putExtra(Config.EXTRA_DEVICE_CONFIGURATION, configuration)
//            return intent
//        }
//    }
//
//
//}