package science.credo.mobiledetector2.activities

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.ndk.CrashlyticsNdk
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import science.credo.mobiledetector2.*
import science.credo.mobiledetector2.camera.Calibration
import science.credo.mobiledetector2.camera.ConfigurationCallback
import science.credo.mobiledetector2.camera.ConfigurationFinder
import science.credo.mobiledetector2.camera.DeviceConfiguration
import science.credo.mobiledetector2.tools.InputFragmentDialog
import science.credo.mobiledetector2.tools.Prefs
import science.credo.mobiledetector2.tools.Tools
import science.credo.mobiledetector2.tools.Tools.Companion.getImageFormatName
import science.credo.mobiledetector2.tools.UIUtils
import science.credo.mobiledetector2.writer.PrefsWriter
import java.lang.NumberFormatException

class StartActivity : AppCompatActivity(), ConfigurationCallback {

    companion object {
        init {
            System.loadLibrary("native-lib")
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fabric.with(this, Crashlytics(), CrashlyticsNdk())

//        btNext.text = getString(R.string.button_permissions)
//        tvMessage.text = getString(R.string.info_permissions)

        btNext.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), Config.RC_PERMISSIONS)
                } else {
                    afterPermissions(true)
                }
            } else {
                afterPermissions(true)
            }
        }
        btNext.performClick()


    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Config.RC_PERMISSIONS) {
            var permissionsGranted = true
            for (result in grantResults) {
                if (result == PackageManager.PERMISSION_DENIED) {
                    permissionsGranted = false
                    break
                }
            }
            afterPermissions(permissionsGranted)

        }
    }

    private fun afterPermissions(permissionsGranted: Boolean) {
//
        btNext.visibility = View.VISIBLE
        if (permissionsGranted) {

            val conf = Prefs.get(this, DeviceConfiguration::class.java)

            if (conf == null) {
                tvMessage.text = getString(R.string.info_configuration)
                btNext.text = getString(R.string.button_configure)
                btNext.setOnClickListener {

                    btNext.visibility = View.GONE
                    tvMessage.text = getString(R.string.info_configuration_in_progress)
                    progressBar.visibility = View.VISIBLE

                    val finder = ConfigurationFinder(this, this)
                    finder.start()
                }
            } else {
                onConfigurationSuccess(conf)
            }


        } else {
            tvMessage.text = getString(R.string.info_permissions)
            btNext.text = getString(R.string.button_permissions)
        }
    }

    override fun onConfigurationLog(warning: String) {


        println(warning)
//        val w = WriteableObject(warning, LogWriter())
//        w.write()


    }

    override fun onConfigurationSuccess(configuration: DeviceConfiguration) {

        GlobalScope.launch(Dispatchers.Main) {
            btNext.visibility = View.VISIBLE
            btNext.text = getString(R.string.button_start)
            tvMessage.text = getString(R.string.info_configuration_success)
            progressBar.visibility = View.GONE
            btNext.setOnClickListener {
                val intent = CalibrationActivity.intent(this@StartActivity, configuration)
                configuration.writer = PrefsWriter(this@StartActivity, DeviceConfiguration::class.java)
                GlobalScope.launch {
                    configuration.write().join()
                }
                startActivity(intent)
                finish()
            }
            displayConfigurationInfo(configuration)
            btChangeExposure.setOnClickListener {
                showEditExposureDialog(configuration)
            }
            tvCoverageThreshold.text = String.format(getString(R.string.coverage_threshold_s,Calibration.noiseThreshold.toString()))
            btChangeThreshold.setOnClickListener {
                showEditThresholdDialog()
            }
        }

    }

    private fun showEditThresholdDialog(){

        val ifd = InputFragmentDialog.newInstance()
        ifd.text = "Enter new coverage threshold (0-255) current: ${Calibration.noiseThreshold}"
        ifd.hint = "threshold"
        ifd.isInputNumericalOnly = true
        ifd.callback = object : InputFragmentDialog.InputDialogCallback {
            override fun onConfirm(v: String) {
                try {
                    val newThreshold : Int = v.toInt()
                    if(newThreshold<0 || newThreshold>255){
                        setError()
                    }else{
                        Calibration.noiseThreshold = newThreshold
                        tvCoverageThreshold.text = String.format(getString(R.string.coverage_threshold_s,Calibration.noiseThreshold.toString()))
                        ifd.dismiss()
                    }
                }catch (e : NumberFormatException){
                    setError()
                }
            }

            override fun onCancel() {

            }
            fun setError(){
                ifd.setError("coverage threshold must be positive number in range 0-255")
            }
        }
        ifd.show(supportFragmentManager,"thresholdInput")

    }



   private fun showEditExposureDialog(configuration: DeviceConfiguration){

        val ifd = InputFragmentDialog.newInstance()
        ifd.text = "Enter new exposure time in millis (max: ${configuration.sensorMaxExposureTime/1000000}ms)"
        ifd.hint = "exposure time in ms"
        ifd.isInputNumericalOnly = true
        ifd.callback = object : InputFragmentDialog.InputDialogCallback {
            override fun onConfirm(v: String) {
                try {
                    val newExposure : Long = v.toLong()
                    if(newExposure>configuration.sensorMaxExposureTime || newExposure<0){
                        setError()
                    }else{
                        configuration.sensorExposureTime = newExposure*1000000
                        if(newExposure*1000000<configuration.avgCalculationTime){
                            configuration.isLossy = true
                        }
                        ifd.dismiss()
                        UIUtils.showAlertDialog(this@StartActivity,getString(R.string.exposure_changed_info))
                        onConfigurationSuccess(configuration)
                    }
                }catch (e : NumberFormatException){
                    setError()
                }
            }

            override fun onCancel() {

            }
            fun setError(){
                ifd.setError("exposure time must be positive number, not higher than ${configuration.sensorMaxExposureTime/1000000}ms!")
            }
        }
        ifd.show(supportFragmentManager,"exposureInput")

    }

    fun displayConfigurationInfo(configuration: DeviceConfiguration) {

        tvImageFormat.text = String.format(getString(R.string.image_format_s, getImageFormatName(configuration.imageFormat)))
        tvFullWidth.text = String.format(getString(R.string.full_width_s, configuration.width.toString()))
        tvFullHeight.text = String.format(getString(R.string.full_height_s, configuration.height.toString()))
        tvAverageCalculationTime.text = String.format(getString(R.string.avg_calculation_time_s, Tools.nanosToMillisString(configuration.avgCalculationTime)))
        tvExposureTime.text = String.format(getString(R.string.exposure_time_s, Tools.nanosToMillisString(configuration.sensorExposureTime)))

        if (configuration.avgCalculationTime < configuration.sensorExposureTime) {
            tvSkippedFrames.text = String.format(getString(R.string.frames_analized, "100%%"))
        } else {
            val ffc = Math.ceil(configuration.avgCalculationTime.toDouble() / configuration.sensorExposureTime)
            tvSkippedFrames.text = String.format(getString(R.string.frames_analized, "${(100 / ffc).toInt()}%%"))
        }
        containerConfiguration.visibility = View.VISIBLE
    }


    override fun onConfigurationError(error: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
