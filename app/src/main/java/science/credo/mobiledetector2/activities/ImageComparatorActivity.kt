package science.credo.mobiledetector2.activities

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageFormat
import android.media.Image
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.Size
import android.view.View
import kotlinx.android.synthetic.main.activity_calibration.*
import kotlinx.android.synthetic.main.activity_comparator.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import science.credo.mobiledetector2.Config
import science.credo.mobiledetector2.R
import science.credo.mobiledetector2.camera.Camera2PostConfiguration
import science.credo.mobiledetector2.camera.CameraUtil
import science.credo.mobiledetector2.camera.DeviceConfiguration
import science.credo.mobiledetector2.camera.Frame
import science.credo.mobiledetector2.resolvers.FrameResult
import science.credo.mobiledetector2.resolvers.ImageFormatBitmapResolver
import science.credo.mobiledetector2.tools.Tools
import science.credo.mobiledetector2.tools.UIUtils
import science.credo.mobiledetector2.writer.FileWriter
import java.lang.reflect.Constructor
import java.util.*

//
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class ImageComparatorActivity : AppCompatActivity(), CameraUtil.FrameCallback {

    private class ImageConfiguration(
            val imageFormat: Int,
            val size: Size,
            val scaledSize: Size,
            val exposureTime: Long?
    ) {
        constructor(imageFormat: Int, size: Size, scaledSize: Size)
                : this(imageFormat, size, scaledSize, null)
    }

    companion object {
        fun intent(context: Context, configuration: DeviceConfiguration)
                : Intent {
            val intent: Intent = Intent(context, ImageComparatorActivity::class.java)
            intent.putExtra(Config.EXTRA_DEVICE_CONFIGURATION, configuration)
            return intent
        }
    }

    private var currentImageConfiguration: ImageConfiguration? = null

    lateinit var configuration: DeviceConfiguration
    lateinit var cameraUtil: CameraUtil
    private val startTimeStamp = System.currentTimeMillis()
    private var counter = 0

    private val desiredImages: Queue<ImageConfiguration> = LinkedList<ImageConfiguration>()

    init {

        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(3968, 2976), Size(3968, 2976), secToNanos(0.05)))
        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(3968, 2976), Size(992, 744), secToNanos(0.05)))
        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(960, 720), Size(960, 720), secToNanos(0.05)))
        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(3968, 2976), Size(1984, 1488), secToNanos(0.05)))
        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(1920, 1080), Size(1920, 1080), secToNanos(0.05)))



//        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(4144, 3106), Size(2072, 1553)))
//        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(2048, 1536), Size(2048, 1536)))
//        desiredImages.add(ImageConfiguration(ImageFormat.JPEG, Size(2048, 1536), Size(2048, 1536)))


//        desiredImages.add(Triple(ImageFormat.RAW_SENSOR, Size(3968, 2976), Size(3968, 2976)))
//        desiredImages.add(Triple(ImageFormat.YUV_420_888, Size(3968, 2976), Size(3968, 2976)))
//        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(4144, 3106), Size(2072, 1553), secToNanos(3.0)))
//        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(2048, 1536), Size(2048, 1536), secToNanos(3.0)))
//
//        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(4144, 3106), Size(2072, 1553), secToNanos(2.0)))
//        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(2048, 1536), Size(2048, 1536), secToNanos(2.0)))
//
//        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(4144, 3106), Size(2072, 1553), secToNanos(1.0)))
//        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(2048, 1536), Size(2048, 1536), secToNanos(1.0)))
//
//        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(4144, 3106), Size(2072, 1553), secToNanos(0.5)))
//        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(2048, 1536), Size(2048, 1536), secToNanos(0.5)))
//
//        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(4144, 3106), Size(2072, 1553), secToNanos(0.25)))
//        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(2048, 1536), Size(2048, 1536), secToNanos(0.25)))
//
//        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(4144, 3106), Size(2072, 1553), secToNanos(0.1)))
//        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(2048, 1536), Size(2048, 1536), secToNanos(0.1)))

//        desiredImages.add(ImageConfiguration(ImageFormat.RAW_SENSOR, Size(4144, 3106), Size(2072, 1553), secToNanos(0.05)))
//        desiredImages.add(ImageConfiguration(ImageFormat.YUV_420_888, Size(2048, 1536), Size(2048, 1536), secToNanos(0.05)))
//        desiredImages.add(ImageConfiguration(ImageFormat.JPEG, Size(2048, 1536), Size(2048, 1536), secToNanos(0.05)))

//        desiredImages.add(Triple(ImageFormat.JPEG, Size(3968, 2976), Size(3968, 2976)))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comparator)
        configuration = intent.getSerializableExtra(Config.EXTRA_DEVICE_CONFIGURATION)
                as DeviceConfiguration

        btBack.setOnClickListener {
            finish()
        }

        startCamera(desiredImages)
    }

    private fun startCamera(queue: Queue<ImageConfiguration>) {
        currentImageConfiguration = queue.poll()
        if (currentImageConfiguration != null) {

            displayProgress("${queue.size}")

            configuration.imageFormat = currentImageConfiguration!!.imageFormat
            configuration.width = currentImageConfiguration!!.size.width
            configuration.height = currentImageConfiguration!!.size.height
            configuration.scaledWidth = currentImageConfiguration!!.scaledSize.width
            configuration.scaledHeight = currentImageConfiguration!!.scaledSize.height
            if (currentImageConfiguration!!.exposureTime != null) {
                configuration.sensorExposureTime = currentImageConfiguration!!.exposureTime!!
                configuration.frameDuration = currentImageConfiguration!!.exposureTime!!
            }


            Log.i("LOG", "==================sensor exposure " + configuration.sensorExposureTime)
            cameraUtil = Camera2PostConfiguration(configuration, this)
            cameraUtil.start(this)
        } else {

            displayProgress("DONE")

        }
    }


    override fun onFrameReceived(frame: Frame) {

        println("==================== $counter   ${frame.imageFormat}    ${frame.byteArray.size}")
        if (counter == 3) {
            cameraUtil.stop()
            counter = 0
            val resolver = ImageFormatBitmapResolver()
            val frameResult = FrameResult(
                    resolver,
                    FileWriter(
                            Tools.getDirName(startTimeStamp),
                            "${Tools.getImageFormatName(currentImageConfiguration!!.imageFormat)}" +
                                    "_${currentImageConfiguration!!.scaledSize.width}" +
                                    "x${currentImageConfiguration!!.scaledSize.height}" +
                                    "_E:${currentImageConfiguration!!.exposureTime}"
                    )
            )
            GlobalScope.launch {
                frameResult.resolve(frame, configuration)
                frameResult.write().join()
                delay(1000)
                startCamera(desiredImages)
            }
        }
        counter++
//        displayProgress("${configuration.imageFormat} c:$counter")

    }

    fun displayProgress(string: String) {
        GlobalScope.launch(Dispatchers.Main) {
            tvProgress.text = string
        }
    }

    override fun onDestroy() {
        cameraUtil.stop()
        super.onDestroy()
    }

    fun secToNanos(s: Double): Long {
        return (s * 1000000000L).toLong()
    }

//    fun secToNanos(s: Double): Long {
//        return (s * 1000000000L).toLong()
//    }
}