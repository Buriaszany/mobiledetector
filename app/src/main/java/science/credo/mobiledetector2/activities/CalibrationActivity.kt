package science.credo.mobiledetector2.activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_calibration.*
import kotlinx.coroutines.*
import science.credo.mobiledetector2.Config
import science.credo.mobiledetector2.R
import science.credo.mobiledetector2.camera.*
import science.credo.mobiledetector2.resolvers.CalibrationResolver
import science.credo.mobiledetector2.tools.Tools
import science.credo.mobiledetector2.tools.UIUtils

//
class CalibrationActivity : AppCompatActivity(), CalibrationFinder.CalibrationCallback {


    override fun onCalibrationFailed() {
       UIUtils.showAlertDialog(this,getString(R.string.calibration_failed))
    }


    override fun onCalibrationSuccess(clusterFactorWidth: Int, clusterFactorHeight: Int, detectionThreshold: Int, avgNoise: Int) {
        configuration.scaledHeight = configuration.height / clusterFactorHeight
        configuration.scaledWidth = configuration.width / clusterFactorWidth
        Calibration.detectionThreshold = detectionThreshold
        Calibration.calibrationNoise = avgNoise
        startDetectorActivity()
    }


    override fun onStatusChanged(status: Int, msg: String, progress: Int,avgNoise: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            when (status) {
                CalibrationFinder.STATUS_COVER_ERROR -> {
                    showCoverInfo(avgNoise.toString())
                }
                CalibrationFinder.STATUS_IN_PROGRESS -> {
                    showInProgressInfo(msg, progress,avgNoise)
                }
            }
        }
    }

    lateinit var configuration: DeviceConfiguration
    private lateinit var calibrationFinder :CalibrationFinder
//    private lateinit var calibrationResolver: CalibrationResolver

    private var progressCounter = 0

    companion object {
        public var isActive = false
        fun intent(context: Context, configuration: DeviceConfiguration)
                : Intent {
            val intent: Intent = Intent(context, CalibrationActivity::class.java)
            intent.putExtra(Config.EXTRA_DEVICE_CONFIGURATION, configuration)
            return intent
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        isActive = false
        calibrationFinder.stop()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isActive = true
        setContentView(R.layout.activity_calibration)
        progressBar.max = CalibrationFinder.LENGHT

        configuration = intent.getSerializableExtra(Config.EXTRA_DEVICE_CONFIGURATION)
                as DeviceConfiguration

        showCoverInfo("starting")
        showConfigurationInfo()



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            calibrationFinder = CalibrationFinder(this, configuration, this)


            GlobalScope.async {
                delay(2000)
                calibrationFinder.start()

//                configuration.scaledHeight = configuration.height / 2
//                configuration.scaledWidth = configuration.width / 4
//                startDetectorActivity()

            }

        } else {

        }

//        cameraUtil = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Camera2PostConfiguration(configuration, this)
//        } else {
//            OldCameraUtil(this)
//        }

//        calibrationResolver = CalibrationResolver(configuration)

//
//        cameraUtil.start(this)
    }

//    override fun onFrameReceived(frame: Frame) {
//
//        val frameResult = FrameResult(calibrationResolver)
//        GlobalScope.launch {
//            val time = System.nanoTime();
//            frameResult.resolve(frame, configuration)
//            println("====== calc time ${(System.nanoTime() - time) / 1000000000.0}")
//            if (frameResult.metadata != null
//                    && frameResult.metadata!!.avg > Calibration.noiseThreshold) {
//
//                showCoverInfo()
//            } else {
//
//                showInProgressInfo()
//                if (frameResult.intArray != null) {
//                    cameraUtil.stop()
//                    startDetectorActivity()
//                }
//            }
//            showProgress(calibrationResolver.counter)
//
//
//        }
//    }

    private fun startDetectorActivity() {
        val intent = DetectorActivity.intent(this, configuration)
        startActivity(intent)
        finish()
    }


    private fun showInProgressInfo(msg: String, progress: Int,avgNoise: Int) {
        GlobalScope.launch(Dispatchers.Main) {
            tvStatus.text = String.format(
                    getString(R.string.info_detector_status),
                    getString(R.string.status_on)
            )
            tvMessage.text = "cluster: $msg avgNoise: $avgNoise"
            progressBar.progress = progress

        }
    }

    private fun showConfigurationInfo() {
        GlobalScope.launch(Dispatchers.Main) {
            tvConfiguration.text = "frameWidth: ${configuration.width}" +
                    "\nframeHeight: ${configuration.height}" +
                    "\nimageFormat: ${configuration.imageFormat}" +
                    "\nexposure: ${Tools.nanosToMillisString(configuration.sensorExposureTime)}"+
                    "\nisLossy: ${configuration.isLossy}"
        }
    }

    private fun showCoverInfo(avgNoise: String) {
        GlobalScope.launch(Dispatchers.Main) {
            tvStatus.text = String.format(
                    getString(R.string.info_detector_status),
                    getString(R.string.status_off)
            )
            tvMessage.visibility = View.VISIBLE
            tvMessage.text ="avgNoise: $avgNoise - \n" + getString(R.string.info_detector_not_covered)
        }
    }

    private fun showProgress(progress: Int) {
        GlobalScope.launch(Dispatchers.Main) {
            progressBar.progress = progress * 100 / CalibrationResolver.CALIBRATION_LEVEL
        }
    }


}