package science.credo.mobiledetector2.activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_detector.*
import kotlinx.android.synthetic.main.activity_detector.btChangeThreshold
import kotlinx.android.synthetic.main.activity_detector.tvExposureTime
import kotlinx.android.synthetic.main.activity_detector.tvFullHeight
import kotlinx.android.synthetic.main.activity_detector.tvFullWidth
import kotlinx.android.synthetic.main.activity_detector.tvImageFormat
import kotlinx.android.synthetic.main.activity_detector.tvMessage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import science.credo.mobiledetector2.Config
import science.credo.mobiledetector2.R
import science.credo.mobiledetector2.camera.*
import science.credo.mobiledetector2.resolvers.*
import science.credo.mobiledetector2.tools.InputFragmentDialog
import science.credo.mobiledetector2.tools.Prefs
import science.credo.mobiledetector2.tools.Tools
import science.credo.mobiledetector2.writer.FileWriter
import java.lang.Exception
import java.lang.NumberFormatException

class DetectorActivity : AppCompatActivity(), CameraUtil.FrameCallback {


    lateinit var configuration: DeviceConfiguration
    lateinit var detectionResolver: BaseResolver
    lateinit var cameraUtil: CameraUtil


    companion object {
        fun intent(context: Context, configuration: DeviceConfiguration)
                : Intent {
            val intent: Intent = Intent(context, DetectorActivity::class.java)
            intent.putExtra(Config.EXTRA_DEVICE_CONFIGURATION, configuration)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detector)
        configuration = intent.getSerializableExtra(Config.EXTRA_DEVICE_CONFIGURATION)
                as DeviceConfiguration

        detectionResolver = DetectionResolver()


        showCoverInfo()
        showCounterInfo(
                Prefs.get(
                        this,
                        Int::class.java,
                        Prefs.DETECTION_COUNTER
                ) ?: 0
        )
        showConfigurationInfo()


        btClearCounter.setOnClickListener {
            Prefs.put(
                    this,
                    "0",
                    Int::class.java,
                    Prefs.DETECTION_COUNTER
            )
            showCounterInfo(0)
        }

        cameraUtil = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Camera2PostConfiguration(configuration, this)
        } else {
            OldCameraUtil(this)
        }


        cameraUtil.start(this)
    }

    private fun showInProgressInfo(avg: Int) {
        GlobalScope.launch(Dispatchers.Main) {
            tvStatus.text = String.format(
                    getString(R.string.info_detector_status),
                    getString(R.string.status_on)
            ) +
                    "\nAvg noise : $avg"
            tvMessage.visibility = View.GONE
        }
    }

    private fun showConfigurationInfo() {
        GlobalScope.launch(Dispatchers.Main) {

            tvImageFormat.text = String.format(getString(R.string.image_format_s, Tools.getImageFormatName(configuration.imageFormat)))
            tvFullWidth.text = String.format(getString(R.string.full_width_s, configuration.width.toString()))
            tvFullHeight.text = String.format(getString(R.string.full_height_s, configuration.height.toString()))
            tvScaledWidth.text = String.format(getString(R.string.scaled_width_s, configuration.scaledWidth.toString()))
            tvScaledHeight.text = String.format(getString(R.string.scaled_height_s, configuration.scaledHeight.toString()))
            val cluster = "${configuration.width / configuration.scaledWidth}x${configuration.height / configuration.scaledHeight}"
            tvClusterSize.text = String.format(getString(R.string.scaled_height_s, cluster))
            tvConfigurationNoise.text = String.format(getString(R.string.configuration_noise_s, Calibration.calibrationNoise.toString()))
            tvDetectionThreshold.text = String.format(getString(R.string.detection_threshold_s, Calibration.detectionThreshold.toString()))

            btChangeThreshold.setOnClickListener {
                showEditThresholdDialog()
            }
        }
    }

    private fun showEditThresholdDialog() {

        val ifd = InputFragmentDialog.newInstance()
        ifd.text = "Enter new detection threshold (0-255) current: ${Calibration.detectionThreshold}"
        ifd.hint = "threshold"
        ifd.isInputNumericalOnly = true
        ifd.callback = object : InputFragmentDialog.InputDialogCallback {
            override fun onConfirm(v: String) {
                try {
                    val newThreshold: Int = v.toInt()
                    if (newThreshold < 0 || newThreshold > 255) {
                        setError()
                    } else {
                        Calibration.detectionThreshold = newThreshold
                        tvDetectionThreshold.text = String.format(getString(R.string.detection_threshold_s, Calibration.detectionThreshold.toString()))
                        ifd.dismiss()
                    }
                } catch (e: NumberFormatException) {
                    setError()
                }
            }

            override fun onCancel() {

            }

            fun setError() {
                ifd.setError("detection threshold must be positive number in range 0-255")
            }
        }
        ifd.show(supportFragmentManager, "thresholdInput")

    }


    private fun showCoverInfo() {
        GlobalScope.launch(Dispatchers.Main) {
            tvStatus.text = String.format(
                    getString(R.string.info_detector_status),
                    getString(R.string.status_off)
            )
            tvMessage.visibility = View.VISIBLE
            tvMessage.text = getString(R.string.info_detector_not_covered)
        }
    }

    private fun showCounterInfo(counter: Int) {
        GlobalScope.launch(Dispatchers.Main) {
            tvCounter.text = String.format(
                    getString(R.string.info_detector_counter),
                    counter
            )
            tvDetectionThreshold.text = String.format(getString(R.string.detection_threshold_s, Calibration.detectionThreshold.toString()))
        }
    }

    fun displayExposureTime(exposure: Long) {
        GlobalScope.launch(Dispatchers.Main) {
            tvExposureTime.text = String.format(getString(R.string.exposure_time_s, Tools.nanosToMillisString(exposure)))
        }
    }

    var counter = 0
    override fun onFrameReceived(frame: Frame) {

        if (detectionResolver.isBusy) {
            return
        }

        val frameResult = FrameResult(
                detectionResolver,
                null
        )
//        if(counter<10){
//            counter++
//            return
//        }else if(counter==11){
//            counter++
//        }else {
//            counter++
//            GlobalScope.launch(Dispatchers.Main) {
//                tvImageFormat.text="no powinno byc"
//            }
//            return
//        }

        GlobalScope.launch {
            displayExposureTime(frame.exposureTime)
            frameResult.resolve(frame, configuration)
            detectionResolver.isBusy = false
            if (frameResult.metadata != null) {
                frameResult.writer = FileWriter(
                        Tools.getDirName(frame.timestamp, frameResult.metadata!!.max - Calibration.detectionThreshold),
                        "hit_${frame.timestamp}"
                )
                if (frameResult.metadata!!.avg > Calibration.calibrationNoise) {
                    if (!CalibrationActivity.isActive) {
                        startActivity(
                                CalibrationActivity.intent(this@DetectorActivity, configuration)
                        )
                        finish()
                    }
                }
                if (frameResult.metadata!!.avg > Calibration.noiseThreshold) {
                    showCoverInfo()
                } else {
                    showInProgressInfo(frameResult.metadata!!.avg)
                    if (frameResult.metadata!!.hit) {
                        val counter = (Prefs.get(
                                this@DetectorActivity,
                                Int::class.java,
                                Prefs.DETECTION_COUNTER) ?: 0) + 1
                        Prefs.put(
                                this@DetectorActivity,
                                counter.toString(),
                                Int::class.java,
                                Prefs.DETECTION_COUNTER
                        )
                        showCounterInfo(counter)

                        frameResult.write()
                        println("================WRITE")
//                    try {
//                        val original = BitmapUtils.createBitmap(frameResult.intArray!!, 2, frame.width, frame.height).await()
//                        frameResult.writer!!.write(original)
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
                    }
                }
            }
            println("========== ${frameResult.intArray}  ${frameResult.resultBitmap}  ${frameResult.metadata}")
        }

    }

    override fun onDestroy() {

        cameraUtil.stop()

        super.onDestroy()

    }
}