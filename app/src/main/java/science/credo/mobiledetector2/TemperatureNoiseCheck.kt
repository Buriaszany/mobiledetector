package science.credo.mobiledetector2

import android.content.Context
import android.graphics.ImageFormat
import android.media.ImageReader
import android.os.Environment
import android.os.Handler
import android.support.annotation.RequiresApi
import android.util.Log
import java.io.*
import java.lang.Runnable
//
//@RequiresApi(api = 21)
//class TemperatureNoiseCheck(
//        conf: Camera2ConfigurationUtil.DeviceConfiguration,
//        private val desiredWidth: Int,
//        private val desiredHeight: Int,
//        private val callback: Callback) : Camera2PostConfiguration(conf) {
//
//    interface Callback {
//        fun onFinished()
//    }
//
//    var counter = 0;
//    val pixelPrecision = if (conf.imageFormat == ImageFormat.RAW_SENSOR) 2 else 1
//    var lastNoises: Noises? = null
//    val fos = openFile()
//    val h = Handler()
//
//
//    private fun openFile(): FileOutputStream {
//        val file_path = Environment.getExternalStorageDirectory().absolutePath + "/detections"
//        val dir = File(file_path)
//        if (!dir.exists())
//            dir.mkdirs()
//        val name = getImageFormatName(conf.imageFormat) +
//                "_${desiredWidth}x${desiredHeight}_" +
//                System.currentTimeMillis()
//        val file = File(dir, "$name.txt")
//        return FileOutputStream(file)
//    }
//
//    override fun stop() {
//        super.stop()
//        fos.flush()
//        fos.close()
//        h.removeCallbacksAndMessages(null)
//    }
//
//    override fun start(context: Context) {
//        super.start(context)
//
//        val T = 30000L
//        val saverRunnable = object : Runnable {
//            override fun run() {
//                counter++
//
//                val record: String = if (lastNoises != null) {
//                    "$counter;" +
//                            "${BatteryStateReceiver.temp};" +
//                            "${lastNoises?.noiseAvg};" +
//                            "${lastNoises?.noise40};" +
//                            "${lastNoises?.noise125};" +
//                            "${lastNoises?.noise190};" +
//                            "${lastNoises?.noise230};" +
//                            "${lastNoises?.noise255};" +
//                            "${lastNoises?.calculationTime}\n"
//                } else {
//                    "----\n"
//                }
//                fos.write(record.toByteArray())
//                lastNoises = null
//                h.postDelayed(this, T)
//            }
//        }
//        h.postDelayed(saverRunnable, T)
//    }
//
//
//    override fun onImageAvailable(p0: ImageReader?) {
//        val image = p0!!.acquireLatestImage()
//        var width = 0
//        var height = 0
//        var imageFormat: Int
//        if (image != null) {
//            width = image.width
//            height = image.height
//            imageFormat = p0.imageFormat
//            val buffer1 = image.planes[0].buffer
//            var data1: ByteArray? = null
//            data1 = ByteArray(buffer1.remaining())
//            buffer1.get(data1)
//            image.close()
//            lastNoises = Noises.calculateNoises(data1, pixelPrecision)
//            println("====255===> ${lastNoises?.noise255} ‰")
//            println("====230===> ${lastNoises?.noise230} ‰")
//            println("====190===> ${lastNoises?.noise190} ‰")
//            println("====125===> ${lastNoises?.noise125} ‰")
//            println("====40===> ${lastNoises?.noise40} ‰")
//            println("====avg===> ${lastNoises?.noiseAvg} ")
//            println("====CT===> ${lastNoises?.calculationTime} ns")
//        }
//    }
//
//    data class Noises(
//            val noiseAvg: Double,
//            val noise255: Double,
//            val noise230: Double,
//            val noise190: Double,
//            val noise125: Double,
//            val noise40: Double,
//            val calculationTime: Long) {
//        companion object {
//            fun calculateNoises(data: ByteArray, pixelPrecision: Int): Noises {
//
//                var sum: Long = 0
//                var noise255 = 0.0
//                var noise230 = 0.0
//                var noise190 = 0.0
//                var noise125 = 0.0
//                var noise40 = 0.0
//
//                val time = System.nanoTime()
//
//                for (i in 0 until data.size step pixelPrecision) {
//                    val byte = data[i].toPositiveInt()
//                    //histogram[byte]++
//                    if (byte > 0) {
//                        sum += byte
//                        if (byte > 40) {
//                            noise40++
//                        }
//                        if (byte > 125) {
//                            noise125++
//                        }
//                        if (byte > 190) {
//                            noise190++
//                        }
//                        if (byte > 230) {
//                            noise230++
//                        }
//                        if (byte == 255) {
//                            noise255++
//                        }
//                    }
//                }
//
//
//
//                noise40 = ((noise40 * 1000.0) / (data.size / pixelPrecision))
//                noise125 = ((noise125 * 1000.0) / (data.size / pixelPrecision))
//                noise190 = ((noise190 * 1000.0) / (data.size / pixelPrecision))
//                noise230 = ((noise230 * 1000.0) / (data.size / pixelPrecision))
//                noise255 = ((noise255 * 1000.0) / (data.size / pixelPrecision))
//
//                return Noises(
//                        sum / (data.size / pixelPrecision.toDouble()),
//                        noise255,
//                        noise230,
//                        noise190,
//                        noise125,
//                        noise40,
//                        System.nanoTime() - time
//                )
//            }
//        }
//    }
//}